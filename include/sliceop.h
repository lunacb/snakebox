#ifndef SLICEOP_H
#define SLICEOP_H

#include <ctype.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

#include "slice.h"

static inline char *compile_str_len(struct slice *list, const size_t list_len)
{
	size_t i, len = 0;
	for(i = 0; i < list_len; i++)
		len += list[i].len;
	// terminating null byte
	len += 1;

	char *str = xmalloc(len);

	size_t pos = 0;
	for(i = 0; i < list_len; i++) {
		memcpy(&str[pos], list[i].start, list[i].len);
		pos += list[i].len;
	}
	str[pos] = '\0';

	return str;
}

#define compile_str(list) \
	compile_str_len(list, sizeof(list) / sizeof(struct slice))

#define str_slice(str) \
	{ .start = str, .len = strlen(str) }

static inline void slice_shift(struct slice *slice, size_t by)
{
	slice->start += by;
	slice->len -= by;
}

static inline bool slice_has_suffix(struct slice *slice, char *str, size_t len)
{
	if(slice->len > len &&
			slice->start[slice->len - len - 1] != '/' &&
			!strncmp(&slice->start[slice->len - len], str, len)) {
		return true;
	}
	return false;
}

static inline bool slice_rm_suffix(struct slice *slice, char *str, size_t len)
{
	if(slice_has_suffix(slice, str, len)) {
		slice->len -= len;
		return true;
	}
	return false;
}

static inline void slice_basename(struct slice *slice)
{
	char *ptr = slice->start;
	int i;

	for(i = slice->len - 1; i >= 0; i--)
		if(ptr[i] != '/')
			break;
	for(; i >= 0; i--) {
		if(ptr[i] == '/') {
			slice_shift(slice, i + 1);
			return;
		}
	}
}

static inline void slice_dirname(struct slice *slice)
{
	char *ptr = slice->start;

	size_t slash = 0, good_slash = 0;
	for(size_t i = 0; i < slice->len; i++) {
		if(ptr[i] == '/')
			slash = i;
		else
			good_slash = slash;
	}

	// one before the last good slash
	slice->len = good_slash;
}

static inline size_t slice_common(struct slice *first, struct slice *second)
{
	size_t min = (first->len < second->len)?first->len:second->len;

	size_t last_slash = 0;
	for(size_t i = 0; i < min; i++) {
		if(first->start[i] != second->start[i])
			return last_slash;
		else if(first->start[i] == '/')
			last_slash = i + 1;
	}

	return min;
}

static inline struct slice slicetok_init(struct slice *str)
{
	return (struct slice){
		.start = str->start,
		.len = 0,
	};
}

static inline bool slicetok(struct slice *str, struct slice *t)
{
	char *end = &str->start[str->len];
	for(t->start = t->start + t->len; t->start < end; t->start++) {
		if(isspace(*(t->start)))
			continue;
		for(t->len = 0; &t->start[t->len] < end; t->len++)
			if(isspace(t->start[t->len]))
				break;
		return true;
	}
	return false;
}

static inline void slice_trim(struct slice *slice)
{
	size_t i;
	for(i = 0; i < slice->len; i++)
		if(!isspace(slice->start[i]))
			break;

	if(i == slice->len) {
		slice->len = 0;
		return;
	}

	slice_shift(slice, i);

	for(i = slice->len; i > 0; i--)
		if(!isspace(slice->start[i-1]))
			break;

	slice->len = i;
}

#endif // SLICEOP_H
