#ifndef OUTPUT_COMMON_H
#define OUTPUT_COMMON_H

#include "interface.h"
#include "slice.h"

enum path_embed_type {
	EmbedLink,
	EmbedImage,
	EmbedVideo,
	EmbedSound,
};

char *resolve_path(struct slice path, enum outputs output, bool *is_path,
		bool *is_doc, bool do_prefix);

#endif // OUTPUT_COMMON_H
