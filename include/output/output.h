#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdbool.h>
#include "document.h"
#include "util.h"

// == html == //
bool output_html_process(char *path, struct doc_file *doc_file,
			struct array *buf);

// == gemini == //
bool output_gemini_process(char *path, struct doc_file *doc_file,
			struct array *buf);

// == gopher == //
bool output_gopher_process(char *path, struct doc_file *doc_file,
			struct array *buf);

// == rss == //
bool output_rss_process(char *path, struct doc_file *doc_file,
			struct array *buf);

#endif // OUTPUT_H
