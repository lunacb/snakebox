#ifndef LOG_H
#define LOG_H

#ifdef TEST
#define _log_file stdout
#else
#define _log_file stderr
#endif

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "main.h"

#define LOG_ERR(x, ...) \
	fprintf(_log_file, "%s: error: " x "\n", program_name, ##__VA_ARGS__)
#define LOG_WARN(x, ...) \
	fprintf(_log_file, "%s: warning: " x "\n", program_name, ##__VA_ARGS__)
#define LOG_ERRNO(x, ...) \
	fprintf(_log_file, "%s: error: " x ": %s\n", program_name, ##__VA_ARGS__, strerror(errno))


#endif
