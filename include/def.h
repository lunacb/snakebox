#define DOC_SUFFIX ".snx"

#define VAR_DOC_BEGIN '|'
#define VAR_DOC_NAMEVAL_DELIM ':'

#define VAR_TMPL_BORDER '_'
#define VAR_TMPL_BORDER_COUNT 2

#define VAR_CHAR_IS_VALID(c) \
	(((c) >= 'A' && (c) <= 'Z') || \
	 ((c) >= 'a' && (c) <= 'z') || \
	 ((c) >= '0' && (c) <= '9') || \
	 (c) == '-')

#define DOC_STACK_MAX 512
