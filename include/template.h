#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include "util.h"

enum template_segtype {
	TemplateText,
	TemplateVar,
	TemplateArray,
};

struct template_segment {
	enum template_segtype type;
	union {
		struct slice text;
		struct {
			char *name;
		} var;
		struct {
			char *name;
			size_t last;
			bool have_delim;
			size_t delim_start;
		} array;
	};
};

struct template_ctx {
	struct array in;
	struct array segments; // struct template_segment
};

struct template_walker {
	struct template_ctx *ctx;
	// If NULL, we're at EOF.
	struct template_segment *pos;
	// Range of used segments.
	struct template_segment *first, *end;
	// first if no delimeter.
	struct template_segment *delim_start;
};

bool template_init(struct template_ctx *ctx, FILE *in);
void template_deinit(struct template_ctx *ctx);
struct template_walker template_walker_init_array(struct template_ctx *ctx,
		struct template_segment *array);
struct template_walker template_walker_init(struct template_ctx *ctx);
struct template_segment *template_walk(struct template_walker *walker);

#endif // TEMPLATE_H
