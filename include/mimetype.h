#ifndef MIMETYPE_H
#define MIMETYPE_H

#include "slice.h"

// TODO: Order similar enum types like this.
enum mimetype {
	MimetypeAudio = 0,
	MimetypeImage,
	MimetypeVideo,

	MimetypeLast,
	MimetypeNone,
};

static char *mimetype_names[MimetypeLast] = {
	[MimetypeAudio] = "audio",
	[MimetypeImage] = "image",
	[MimetypeVideo] = "video",
};

static inline enum mimetype get_mimetype(struct slice *slice) {
	for(enum mimetype i = 0; i < MimetypeLast; i++)
		if(!strncmp(slice->start, mimetype_names[i], slice->len))
			return i;

	return MimetypeNone;
}

#endif // MIMETYPE_H
