#ifndef SLICE_H
#define SLICE_H

struct slice {
	char *start;
	size_t len;
};

#endif // SLICE_H
