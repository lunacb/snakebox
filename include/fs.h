#ifndef FS_H
#define FS_H

#include <stdbool.h>
#include "interface.h"

struct global_paths {
	struct {
		char *dest_from_cwd;
		char *templates[TemplateLast];
	} output[OutputLast];

	char *src_from_cwd;
};

typedef bool (*fs_source_func_t)(char *path);

void fs_init(void);
void fs_deinit(void);
bool fs_add_template_dir(char *dir);
bool fs_add_output_template_dir(char *output, char *dir);
bool fs_add_output_template(char *output, char *name, char *file);
bool fs_set_output_dest(char *output_name, char *dir);
bool fs_set_dest(char *dir);
bool fs_set_prefix(char *output, char *new_prefix);
bool fs_walk_source(fs_source_func_t func);
char *fs_doc_to_dest(struct slice str, enum outputs output, bool *is_doc,
		bool do_prefix);
bool fs_make_basedir(char *path);
char *fs_path_to_dest(enum outputs output, char *path);

// Path of a source snx file (from cwd) to a returned destination file (from
// cwd) and a path local to an the output put in local_path. The returned
// pointer is owned by the calling function, but not local_path.
char *fs_src_to_dest_path(enum outputs output, char *path, char **local_path);

char *fs_path_file_to_dest_path(enum outputs output, char *path);

char *fs_doc_gen_relative_path(enum outputs output, char *from, char *to,
		bool from_dir);

extern char *prefix[OutputLast];

extern char *index_file;
extern char *tag_index_file;
extern char *rss_file;
extern char *tag_single_dir;
extern char *path_files[PathFileLast];

extern struct global_paths gpaths;

#endif // FS_H
