#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdbool.h>
#include <string.h>
#include "document.h"
#include "util.h"

enum outputs {
	OutputHtml,
	OutputGemini,
	OutputGopher,
	OutputRss,
	OutputLast,
};

enum targets {
	TargetDocument,
	TargetIndex,
	TargetTagIndex,
	TargetLast,
};

enum templates {
	TemplateDoc,
	TemplateIndex,
	TemplateTagIndex,
	TemplateTagSingleIndex,
	TemplateLast,
};

enum path_files {
	PathFileIndex,
	PathFileTagIndex,
	PathFileTagDir,
	PathFileSingleTag,
	PathFileDoc,
	PathFileLast,
};

struct names {
	char *target[TargetLast];
	char *output[OutputLast];
	char *template[TemplateLast];
};

struct path_opt {
	enum path_files file;
	char *opt_name;
	char *help;
	char *value;
};

struct path_varname {
	char *absolute;
	char *relative;
};

typedef bool (*output_process_func_t)(char *path, struct doc_file *doc_file,
		struct array *buf);

struct output_info {
	// used for text inside a document
	char *suffix;
	// used for deciding the filepath to write to
	char *file_suffix;
	output_process_func_t func_process;
};

struct file_meta;

typedef bool (*target_init_func_t)(void);
typedef void (*target_deinit_func_t)(void);
typedef void (*target_deinit_err_func_t)(void);
typedef bool (*target_process_func_t)(enum outputs output, struct file_meta *meta);

struct target_info {
	enum templates *templates;
	target_init_func_t func_init;
	target_deinit_func_t func_deinit;
	target_deinit_err_func_t func_deinit_err;
	target_process_func_t func_process;
};

extern const struct names names;
extern const struct output_info output_info[OutputLast];
extern const struct target_info target_info[TargetLast];
extern struct path_opt *path_opts;
extern struct path_varname path_varnames[PathFileLast];

static inline int enumerate_name(char *name, char *const *list, int end)
{

	int var;

	for(var = 0; var < end && strcmp(list[var], name); var++)
		;

	return var;
}

// same as enumerate_name, but sets var to enumerated value and returns
// true/false for success/failure
#define try_enumerate(var, name, list, end) \
	((var = enumerate_name(name, list, end)) != end) \


#endif // INTERFACE_H
