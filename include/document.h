#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include "def.h"
#include "slice.h"
#include "util.h"

struct doc_var {
	size_t name;
	size_t val;
};

struct doc_file {
	struct array vars; // doc_var
	struct array head; // char
	size_t rest;
	char *present;
	char *filename;
	int line;
};

enum doc_token_type {
	DocBreak, DocCode, DocEmbed, DocHeading, DocList, DocOrderedList, DocListItem,
	DocOrderedItem, DocPath, DocQuote, DocText, DocParagraph,
	// for internal use
	DocNone,
};

enum doc_text_flags {
	DocTextBold   = 1 << 0,
	DocTextItalic = 1 << 1,
	DocTextCode   = 1 << 2,
};

enum doc_checkbox {
	DocNoCheckbox,
	DocChecked,
	DocUnchecked,
};

struct doc_token {
	enum doc_token_type type;

	union {
		struct {
			enum doc_text_flags flags;
			enum doc_text_flags full_flags;
			struct slice text;
		} text;

		struct {
			int number;
		} ordered;

		struct {
			struct slice path;
		} path;

		struct  {
			struct slice path;
			struct slice mime_type;
			struct slice mime_subtype;
		} embed;

		struct {
			int level;
		} heading;
	};
};

enum doc_end {
	DocEndStrong,
	DocEndWeak,
};

struct doc_stack_token {
	enum doc_end end;
	struct doc_token token;
};

struct doc_iter {
	struct slice buf;
	size_t pos;
	size_t linescan;
	int line;

	bool graph_this_line;
	size_t graph_at;

	int stack_len;
	struct doc_stack_token stack[DOC_STACK_MAX];

	bool stack_add_pending;
	int stack_remove_pending;
	struct doc_stack_token pending;

	char *filename;

	int quote_level;

	char *present;
};

struct doc_file doc_file_init(void);
void doc_file_deinit(struct doc_file *doc_file);
struct doc_file doc_file_shallow_copy(struct doc_file *src);
bool doc_file_load(FILE *file, char *filename, struct doc_file *doc_file);
struct doc_iter doc_make_iter_slice(struct slice slice, int line, char *filename,
		char *present);
struct doc_iter doc_make_iter(struct doc_file *doc_file);
int doc_iter_walk(struct doc_iter *iter, struct doc_stack_token **tok_ptr,
		bool *is_reused);
bool doc_match_var(char *match, struct doc_file *doc_file, size_t *where);

#define doc_file_head(doc_file) \
	((char *)(doc_file).head.data)

#define doc_file_var(doc_file, i, field) \
	&doc_file_head(doc_file)[((struct doc_var *)(doc_file).vars.data)[i].field]

#define doc_file_rest(doc_file) \
	&doc_file_head(doc_file)[(doc_file).rest]

#endif
