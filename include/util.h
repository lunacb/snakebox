#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>

struct array {
	void *data;
	size_t len;
	size_t cap;
	size_t size; // size of a single member
};

struct array array_init(size_t size);
void array_atleast_cap(struct array *array, size_t cap);
void *array_add(struct array *array, size_t n);
void array_rm_messy(struct array *array, size_t i);

void *alloc_test(const char *file, int line, void *ptr);

#define xmalloc(size) alloc_test(__FILE__, __LINE__, malloc(size))
#define xrealloc(ptr, size) alloc_test(__FILE__, __LINE__, realloc(ptr, size))
#define xcalloc(ptr, size) alloc_test(__FILE__, __LINE__, calloc(nmemb, size))
#define xstrdup(ptr) alloc_test(__FILE__, __LINE__, strdup(ptr))
#define xstrndup(ptr, len) alloc_test(__FILE__, __LINE__, strndup(ptr, len))

#define array_add_str(array, str, len) \
	memcpy(array_add(array, len), str, len)

#define array_add_lit(array, lit) \
	memcpy(array_add(array, sizeof(lit) - 1), (char *)lit, sizeof(lit) - 1)

#define MIN(x, y) (x<y?x:y)

#endif // UTIL_H
