#ifndef MAIN_H
#define MAIN_H

#include "interface.h"
#include "template.h"

extern bool input_ok;
extern char *program_name;

extern struct template_ctx template_map[OutputLast][TemplateLast];
extern bool used_targets[TargetLast];
extern bool used_outputs[OutputLast];

extern char *date_read_fmt;
extern char *date_write_fmt;

#endif
