#ifndef TARGET_COMMON_H
#define TARGET_COMMON_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "document.h"
#include "fs.h"
#include "interface.h"
#include "template.h"
#include "util.h"

#define VAR_STACK_MAX 24

struct time_info {
	struct tm tm;
	struct timespec exact;
	char str[128];
};

struct file_meta {
	struct time_info created;
	struct time_info modified;
	char *src_path, *dest_path, *local_path;
	struct doc_file doc_file;
	char *body;
};

enum var_stack_type {
	VarStackValue,
	VarStackDoc,
	VarStackPathFile,
};

struct var_stack_item {
	enum var_stack_type type;
	union {
		struct {
			char *key;
			char *value;
		} value;

		struct {
			struct file_meta *meta;
		} doc;

		struct {
			enum path_files file;
			char *value;
		} path_file;
	};
};

struct var_stack {
	struct var_stack_item items[VAR_STACK_MAX];
	int len;

	enum path_files path_file;
	bool is_dir;

	char *to_free;
};

struct var_stack_pointer {
	int start;
};

bool file_meta_init(struct file_meta *meta, char *src_path, char *dest_path,
		char *local_path, char *body, struct doc_file doc_file);
int meta_compar(const void *first, const void *second);
char *match_path(enum outputs output, char *name, enum path_files src,
		bool from_dir);
bool write_document_from_template(enum outputs output, enum templates template,
		struct file_meta *meta, struct var_stack *stack,
		struct template_walker *walker, struct template_ctx *ctx, FILE *file);
void file_meta_deinit(struct file_meta *meta);
struct file_meta file_meta_clone(struct file_meta *src);

static inline struct var_stack var_stack_init(void)
{
	return (struct var_stack){
		.len = 0,
		.path_file = PathFileLast,
		.is_dir = false,
		.to_free = NULL,
	};
}

static inline struct var_stack_pointer var_stack_get_pointer(struct var_stack *stack)
{
	return (struct var_stack_pointer){ stack->len };
}

static inline void var_stack_set_current_path_file(struct var_stack *stack,
		enum path_files file, bool is_dir)
{
	stack->path_file = file;
	stack->is_dir = is_dir;
}

static inline void var_stack_push_value(struct var_stack *stack, char *key,
		char *value)
{
	assert(stack->len < VAR_STACK_MAX);
	stack->items[stack->len++] = (struct var_stack_item){
		.type = VarStackValue,
		.value = {
			.key = key,
			.value = value,
		}
	};
}

static inline void var_stack_push_doc(struct var_stack *stack,
		struct file_meta *meta)
{
	assert(stack->len < VAR_STACK_MAX);
	stack->items[stack->len++] = (struct var_stack_item){
		.type = VarStackDoc,
		.doc = {
			.meta = meta,
		}
	};
}

static inline void var_stack_push_path_file(struct var_stack *stack,
		enum path_files file, char *value)
{
	assert(stack->len < VAR_STACK_MAX);
	stack->items[stack->len++] = (struct var_stack_item){
		.type = VarStackPathFile,
		.path_file = {
			.file = file,
			.value = value,
		}
	};
}

static inline void var_stack_pop(struct var_stack *stack,
		struct var_stack_pointer *pointer)
{
	stack->len = pointer->start;
}

// Returns a string matched from the given stack or NULL if there was no match.
// Must call var_stack_cleanup after each successful match.
char *var_stack_match(enum outputs output, struct var_stack *stack, char *name);
void var_stack_cleanup(struct var_stack *stack);

#endif // TARGET_COMMON_H
