#ifndef TARGET_H
#define TARGET_H

#include <stdbool.h>
#include "document.h"
#include "interface.h"
#include "target/common.h"
#include "util.h"

// == index == //
bool target_index_init(void);
void target_index_deinit(void);
void target_index_deinit_err(void);
bool target_index_process(enum outputs output, struct file_meta *meta);

// == document == //
bool target_document_init(void);
void target_document_deinit(void);
void target_document_deinit_err(void);
bool target_document_process(enum outputs output, struct file_meta *meta);

// == tag_index == //
bool target_tag_index_init(void);
void target_tag_index_deinit(void);
void target_tag_index_deinit_err(void);
bool target_tag_index_process(enum outputs output, struct file_meta *meta);

#endif // TARGET_H
