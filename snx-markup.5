.Dd Nov 12, 2022
.Dt SNX-MARKUP 5
.Os
.Sh NAME
.Nm snx-markup
.Nd markup format used by snx documents
.Sh DESCRIPTION
An snx markup file is used to write documents to be converted to other,
output-specific formats. The file has a heading section followed by a body
section. The heading section consists of a consecutive sequence of lines at the
beginning of the file, each starting with a '|' character. The first line that
does not start with a '|' character begins the body section.
.Pp
A heading line is ignored if it contains no non-whitespace characters.
Otherwise, the first non-whitespace character of a heading line starts the name
of a variable to set, followed by a ':'. Everything after is the value to set
that variable to. The value has any whitespace trimmed. For example:
.Bd -literal -offset indent
| title: Something About Frogs
|
| tags: science,frog
.Ed

will set the
.Sy "title"
variable to "Something About Frogs", and
.Sy "tags"
to "science,frog". Variables and are intended to provide metadata about a
document, and some are used by targets. All variable names must be valid
template variable names. See
.Xr snx-templates 5
for more details. 
.Pp
The body section contains directives, which are enclosed in square brackets
('[' and ']'), and are followed by any amount of horizontal whitespace, then
a ':' for a strong directive, or a '{' for a weak directive. A '}' explicitly
ends the scope of the last strong directive. A weak directive has an implied
ending that will be explained later. The directive command (within the square
brackets) consists of the directive name followed by zero or more arguments,
all delimited by whitespace. The following example gives two identical
paragraphs:
.Bd -literal -offset indent
[paragraph]: Some text and some more text.

[paragraph] {
  [^normal]: Some text and some more text.
}
.Ed

A text directive is a special type of directive that can only contain literal
text strings and other text directives. Anything  that isn't a directive is a
literal text string. All text directive names start with a '^' character. If a
literal text string in a document is not within the scope of a text directive,
a weak
.Ic ^normal
text directive is implicitly added at the beginning of it, as demonstrated in
the previous two examples.
.Pp
A weak directive will end where a new directive starts, unless the new
directive is a text directive and the old one isn't. This allows text
directives to be embedded within other directives without being too
syntactically noisy. A weak directive's scope also ends if it's parent's scope
ends.
.Pp
In practice, this means that the end of a directive can be given either
explicitly or implicitly, whichever is necessary and/or more visually
coherent, and differently-formatted text can be mixed together:
.Bd -literal -offset indent
[p]:  A paragraph with
some [^b]{bold} text
and some [^i]: italic.

[list] {
  [*]: a list item
  [*]: another list item
}
.Ed
.Pp
Unless it is a code directive, a literal tex string is delimited by whitespace,
the specific whitespace characters used doesn't matter. An output will format
it to make it look pretty. That allows you to format the source file however
you want, such as by wrapping lines with newline characters. An output will
consider two consecutive text directives to be delimited by whitespace, even if
none exists between them in the markup document.
.Sh SPECIAL VARIABLES
Some heading section variables have special meaning to the program, or are
commonly used for a specific purpose.
.Bl -tag -width Dn
.It Ic tags
This should be a comma-and-whitespace-separated list of tags. Each tag must
follow the same naming standards as variables themselves. This is used by the
.Ic tag_index
target.
.It Ic title
This can be used to give a page or blog post a title to be used in a template.
.It Ic modified
This is the time the document was last modified. If unspecified, it is queried
through the filesystem. The time string must follow a specific format. See
.Xr snx 1
for more details.
.It Ic created
This is the time the document was created. If unspecified, it is the same
as
.Ic modified .
The time string must follow a specific format. See
.Xr snx 1
for more details.
.El
.Sh DIRECTIVES
Directive names are case-insensitive. They all have a shorter (one to three
character) alias. 
.Pp
The directives that can be contained in the root document (aren't contained in
any other directive) are:
.Sy break, heading, paragraph, quote, code, list, ordered, embed, link .
.Bl -tag -width Dn
.It Ic break, b
A vertical line break.
.Pp
.Sy Can contain:
none
.It Ic heading, # Ar level
A section heading with the given
.Ar level .
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic paragraph, p
A paragraph of text.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic quote, q
An extended quotation. This can contain anything the root document does, which
means you can also have nested quotes.
.Pp
.Sy Can contain:
.Sy break, heading, paragraph, quote, code, list, ordered, embed, link .
.It Ic code, c
A code block. Contents will be displayed exactly as given in the markup file in
a monospaced font.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic list, l
An unordered list. Nested lists are not allowed because some output formats
(such as
.Ic gemini )
would not work well with them.
.Pp
.Sy Can contain: item
.It Ic ordered, o
An unordered list. Nested lists are not allowed because some output formats
(such as
.Ic gemini )
would not work well with them.
.Pp
.Sy Can contain: item
.It Ic item, *
A list item in an ordered or unordered list.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic link, => Ar pattern
Points to
.Ic pattern .
The contained text is displayed. See
.Sx LINKS
for the format of the
.Ic pattern .
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic embed, >> Ar pattern Ar mimetype
Same as
.Ic link ,
except that if possible it tries to embed the element pointed to by the link
into the document. The
.Ar mimetype
is used as a hint to help the output decide how to embed. It can be specified
formally as
.Ic type/subtype ,
or informally without a subtype as
.Ic type/*
or
.Ic type .
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic ^normal, ^n
Normal text.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic ^bold, ^b
Bold text.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic ^italic, ^i
Italic text.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic ^bolditalic, ^bi
Bold and italic text.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.It Ic ^code, ^c
Embedded code text. This does not change how the text is formatted.
.Pp
.Sy Can contain:
.Sy ^normal, ^bold, ^italic, ^bolditalic, ^code
.El
.Sh DIRECTIVE ALIASES
.Pp
A few patterns, if found at the beginning of a line, preceded only by
whitespace, and followed by at least one whitespace character, will be
substituted for commonly used implicit directives. The first is any number
of '#' characters, which will be substituted for a
.Ic heading
directive of equivalent level. The second is a '*' character, which will be
substituted for an
.Ic item
directive. The following examples are equivalent:
.Bd -literal -offset indent
# heading 1
## heading 2
[list] {
  * item 1
  * item 2
}

[# 1]: heading 1
[# 2]: heading 2
[list] {
  [*]: item 1
  [*]: item 2
}
.Ed
.Sh LINKS
A link is by default treated as a URL. If it is surrounded in parentheses,
however, it is treated as a local filepath. If this is an absolute path
(starting with a slash), then it will be evaluated relative to the source
directory. Otherwise, it will be evaluated relative to the markup file it is
contained in. If the path looks like a markup file (with a .snx suffix), then
it will be converted to a path that points to the equivalent per-output
destination file. For example:
.Bd -literal -offset indent
[=> (information.snx)]: Points to "information.html" in the html output
[=> https://example.org]: Points to the specified URL
.Ed
.Sh BUGS
The handling of parsing errors is currently strange and unintuitive. You
should get a warning message, but any files generated after one should not be
trusted.
.Sh SEE ALSO
.Xr snx 1
.Xr snx-templates 5
.Pp
.Lk https://codeberg.org/lunacb/snakebox
.Sh AUTHORS
lunacb (lunacb@disroot.org)
