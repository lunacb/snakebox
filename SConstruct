import os

vars = Variables('variables.py', ARGUMENTS)

vars.AddVariables(
        BoolVariable('werror',
            help='Whether warnings should be treated as errors.',
            default=True
        ),
        BoolVariable('verbose',
            help='Wether compiler output should be verbose',
            default=False
        ),
        EnumVariable('target',
            help='Compilation target',
            default='debug',
            allowed_values = ('debug', 'release', 'test')
        ),
        PathVariable('prefix',
            help='Install prefix',
            default='/usr/local',
            validator=PathVariable.PathAccept,
        )
)

env = Environment(variables=vars);
Help(vars.GenerateHelpText(env))

# make the prefix handle relative paths right
env['prefix'] = os.path.realpath(env['prefix'])

env.Decider('MD5-timestamp')

if env['werror']:
    env.MergeFlags('-Werror')

if not(env['verbose']):
    def comstr(color, no_color):
        return "\033[32m%s\033[0m %s" % (color, no_color)
    env.Append(CCCOMSTR=comstr("Compiling", "$TARGET"))
    env.Append(CXXCOMSTR=comstr("Compiling", "$TARGET"))
    env.Append(SHCCCOMSTR=comstr("Compiling shared", "$TARGET"))
    env.Append(SHCXXCOMSTR=comstr("Compiling shared", "$TARGET"))
    env.Append(ARCOMSTR=comstr("Linking static library ", "$TARGET"))
    env.Append(RANLIBCOMSTR=comstr("Ranlib library", "$TARGET"))
    env.Append(SHLINKCOMSTR=comstr("Linking shared library", "$TARGET"))
    env.Append(LINKCOMSTR=comstr("Linking", "$TARGET"))

build_dir = 'build/release'
if env['target'] == 'debug':
    env.MergeFlags('''
        -g
        -O0
        ''')
    build_dir = 'build/debug'
elif env['target'] == 'test':
    env.MergeFlags('''
        -g
        -O0
        ''')
    build_dir = 'build/test'
else:
    env.MergeFlags('-O3')

env.Append(CPPPATH=['#/include'])

env.MergeFlags('''
    -Wall
    -Winvalid-pch
    -Wextra
    -std=c11
    -fdiagnostics-color=always
    ''')

env.Alias('install', env['prefix'])

# make all the directories we'll need for installing
if 'install' in COMMAND_LINE_TARGETS:
    for n in [ '/bin', '/share/man/man1', '/share/man/man5' ]:
        full = env['prefix'] + n
        if not os.path.isdir(full):
            os.makedirs(full)

# install manpages
env.Install(target = env['prefix'] + '/share/man/man1', source = 'snx.1')
env.Install(target = env['prefix'] + '/share/man/man5',
        source = [ 'snx-markup.5', 'snx-templates.5' ] )

Export('env')

if env['target'] == 'test':
    SConscript('tests/SConscript', variant_dir=build_dir, duplicate=False)
else:
    SConscript('src/SConscript', variant_dir=build_dir, duplicate=False)
