#!/bin/sh

scons target=test || exit

cd "build/test/bin"
for f in *; do
	echo "Running test \"$f\"."
	rm -rf /tmp/.test
	if ! ./$f >/dev/null; then
		echo "Test \"$f\" failed!"
		exit
	fi
done
