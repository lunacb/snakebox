#define _POSIX_C_SOURCE 200809L

#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "def.h"
#include "interface.h"
#include "log.h"
#include "main.h"
#include "sliceop.h"
#include "util.h"

#include "fs.h"

char *prefix[OutputLast];

char *index_file = "index";
char *tag_index_file = "tags";
char *rss_file = "feed";
char *tag_single_dir = ".";
char *path_files[PathFileLast];

struct global_paths gpaths;

void fs_init(void)
{
	memset(&gpaths, 0, sizeof(gpaths));

	for(int i = 0; i < OutputLast; i++) {
		for(int j = 0; j < TemplateLast; j++)
			gpaths.output[i].templates[j] = NULL;

		gpaths.output[i].dest_from_cwd = NULL;

		prefix[i] = NULL;
	}

	for(enum path_files i = 0; i < PathFileLast; i++) {
		path_files[i] = NULL;
	}

	for(enum path_files i = 0; path_opts[i].file != PathFileLast; i++) {
		path_files[path_opts[i].file] = path_opts[i].value;
	}
}

static inline bool is_dir(char *file)
{
	struct stat buf;
	if(stat(file, &buf)) {
		LOG_ERRNO("Failed to stat file %s", file);
		return false;
	}
	if(!S_ISDIR(buf.st_mode)) {
		LOG_ERR("File %s is not a directory", file);
		return false;
	}
	return true;
}

bool fs_add_template_dir(char *dir)
{
	char buf[PATH_MAX+1];

	const int bufsize = sizeof(buf);

	if(!is_dir(dir))
		return false;

	for(enum outputs i = 0; i < OutputLast; i++) {
		for(enum templates j = 0; j < TemplateLast; j++) {
			if(snprintf(buf, bufsize, "%s/%s/%s", dir, names.output[i],
						names.template[j]) >= bufsize) {
				LOG_ERR("Buffer size exceeded when adding template dir %s", dir);
				return false;
			}

			if(!access(buf, R_OK)) {
				free(gpaths.output[i].templates[j]);
				gpaths.output[i].templates[j] = xstrdup(buf);
			}
		}
	}

	return true;
}

void fs_deinit(void)
{
	for(int i = 0; i < OutputLast; i++) {
		for(int j = 0; j < TemplateLast; j++)
			free(gpaths.output[i].templates[j]);

		// TODO: don't dynamically allocate if not needed.
		free(gpaths.output[i].dest_from_cwd);
	}
}

bool fs_add_output_template_dir(char *output_name, char *dir)
{
	enum outputs output;

	if(!try_enumerate(output, output_name, names.output, OutputLast)) {
		input_ok = false;
		LOG_ERR("Output %s does not exist", output_name);
		return false;
	}

	if(!is_dir(dir))
		return false;

	char buf[512];

	const int bufsize = sizeof(buf);

	for(enum templates i = 0; i < TemplateLast; i++) {
		if(snprintf(buf, bufsize, "%s/%s", dir, names.template[i]) >= bufsize) {
			LOG_ERR("Buffer size exceeded when adding template dir %s", dir);
			return false;
		}

		if(!access(buf, R_OK)) {
			free(gpaths.output[output].templates[i]);
			gpaths.output[output].templates[i] = xstrdup(buf);
		}
	}

	return true;
}

bool fs_add_output_template(char *output_name, char *name, char *file)
{
	enum templates template;
	enum outputs output;

	if(!try_enumerate(template, name, names.template, TemplateLast)) {
		input_ok = false;
		LOG_ERR("Template %s does not exist", name);
		return false;
	}

	if(!try_enumerate(output, output_name, names.output, OutputLast)) {
		input_ok = false;
		LOG_ERR("Output %s does not exist", output_name);
		return false;
	}

	if(access(file, R_OK) == -1) {
		LOG_ERR("Cannot access file %s", file);
		return false;
	}

	free(gpaths.output[output].templates[template]);
	gpaths.output[output].templates[template] = xstrdup(file);

	return true;
}

bool fs_set_output_dest(char *output_name, char *path)
{
	enum outputs output;

	if(!try_enumerate(output, output_name, names.output, OutputLast)) {
		input_ok = false;
		LOG_ERR("Output %s does not exist", output_name);
		return false;
	}

	free(gpaths.output[output].dest_from_cwd);
	gpaths.output[output].dest_from_cwd = xstrdup(path);

	return true;
}

bool fs_set_dest(char *dir)
{
	char buf[512];

	const int bufsize = sizeof(buf);

	if(!is_dir(dir))
		return false;

	for(enum outputs i = 0; i < OutputLast; i++) {
		if(snprintf(buf, bufsize, "%s/%s", dir, names.output[i]) >= bufsize) {
			LOG_ERR("Buffer size exceeded when adding dest dir %s", dir);
			return false;
		}

		free(gpaths.output[i].dest_from_cwd);
		gpaths.output[i].dest_from_cwd = xstrdup(buf);
	}

	return true;
}

bool fs_set_prefix(char *output_name, char *new_prefix)
{
	enum outputs output;

	if(!try_enumerate(output, output_name, names.output, OutputLast)) {
		input_ok = false;
		LOG_ERR("Output %s does not exist", output_name);
		return false;
	}

	prefix[output] = new_prefix;

	return true;
}

static bool walk_for_dir(fs_source_func_t func, struct array *namebuf,
		bool *got_file);

static bool walk_try(fs_source_func_t func, struct array *namebuf,
		char *d_name)
{
	// determine whether the namefile ends in the mardown filename
	// extension
	size_t len = strlen(d_name);
	char *pos = &d_name[len - sizeof(DOC_SUFFIX) + 1];
	bool maybe_doc = (len > sizeof(DOC_SUFFIX) - 1 && !strcmp(pos, DOC_SUFFIX));

	strcpy(array_add(namebuf, len + 1), d_name);

	bool got_file;
	if(!walk_for_dir(func, namebuf, &got_file))
		return false;

	if(maybe_doc && got_file)
		return func(namebuf->data);

	return true;
}

static bool walk_for_dir(fs_source_func_t func, struct array *namebuf,
		bool *got_file)
{
	char *name = namebuf->data;

	DIR *dir = opendir(name);
	struct dirent *dp;

	*got_file = false;

	if(dir == NULL) {
		if(errno == ENOTDIR) {
			errno = 0;

			*got_file = true;
			return true;
		} else {
			LOG_ERRNO("Failed to open %s", name);
			return false;
		}
	}

	// remember the length of the name, without the terminating null byte, with
	// an appended slash
	size_t base_len = namebuf->len--;
	*((char *)array_add(namebuf, 1)) = '/';

	while((dp = readdir(dir)) != NULL) {
		if(!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue;

		namebuf->len = base_len;

		if(!walk_try(func, namebuf, dp->d_name))
			goto error;
	}

	if(errno) {
		LOG_ERRNO("Failed to read from %s", name);
		return false;
	}

	closedir(dir);
	return true;

error:
	closedir(dir);
	return false;
}

bool fs_walk_source(fs_source_func_t func)
{
	struct array namebuf = array_init(sizeof(char));

	strcpy(array_add(&namebuf, strlen(gpaths.src_from_cwd) + 1),
			gpaths.src_from_cwd);
	bool got_file;
	bool res = walk_for_dir(func, &namebuf, &got_file);

	free(namebuf.data);

	if(got_file) {
		LOG_ERR("Source path %s is a file instead of a directory",
				gpaths.src_from_cwd);
		return false;
	}

	return res;
}

bool fs_make_basedir(char *path)
{
	for(size_t i = 0; path[i]; i++) {
		// skip to the next slash
		if(path[i] != '/')
			continue;

		// skip to the next non-slash char, remember the old index
		size_t old_i = i + 1;
		for(i++; path[i] == '/'; i++)
			;

		// leave at the end of the string
		if(path[i] == '\0')
			break;

		// make the dir we remembered the index of
		char tmp = path[old_i];
		path[old_i] = '\0';
		if(mkdir(path, 0755) == -1) {
			if(errno != EEXIST) {
				LOG_ERRNO("Failed to create directory %s", path);
				path[old_i] = tmp;
				return false;
			}

			errno = 0;

			// TODO: don't use is_dir so we can do a more descriptive error message
			if(!is_dir(path)) {
				path[old_i] = tmp;
				return false;
			}
		}
		path[old_i] = tmp;
	}

	return true;
}

char *fs_doc_to_dest(struct slice str, enum outputs output, bool *is_doc,
		bool do_prefix)
{
	struct slice suffix = str_slice(output_info[output].suffix);
	if(is_doc)
		*is_doc = true;
	if(!slice_rm_suffix(&str, DOC_SUFFIX, sizeof(DOC_SUFFIX) - 1)) {
		suffix.len = 0;
		if(is_doc)
			*is_doc = false;
	}

	if(str.start[0] == '/') {
		return compile_str(((struct slice[]){
			str_slice((do_prefix && prefix[output])?prefix[output]:""),
			str,
			suffix,
		}));
	}

	return compile_str(((struct slice[]){
		str,
		suffix,
	}));
}

static size_t count_components(struct slice *slice)
{
	bool can_count = true;
	size_t count = 0;

	for(size_t i = 0; i < slice->len; i++) {
		if(slice->start[i] == '/') {
			can_count = true;
		} else if(can_count) {
			can_count = false;
			count++;
		}
	}

	return count;
}

static inline void skip_slice_slashes(struct slice *slice)
{
	while(slice->len && slice->start[0] == '/')
		slice_shift(slice, 1);
}

// Creates a path to a file managed by us (index file, etc.)
// TODO: remove from_dir, we don't need it
char *fs_doc_gen_relative_path(enum outputs output, char *from, char *to, bool from_dir)
{
	struct slice suffix = str_slice(output_info[output].suffix);
	struct slice slice1_before = str_slice(from);

	struct slice slice1 = slice1_before;
	struct slice slice2 = str_slice(to);

	size_t common = slice_common(&slice1, &slice2);

	// only operate on the portion of the two that is different
	slice_shift(&slice1, common);
	slice_shift(&slice2, common);

	// sometimes needed
	skip_slice_slashes(&slice1);
	skip_slice_slashes(&slice2);

	size_t diff = count_components(&slice1);
	if(diff == 0 && !from_dir) {
		// should only happen if path and to are the same
		slice_basename(&slice1_before);
		return compile_str(((struct slice[]){
			slice1_before,
			suffix,
		}));
	}

	// ignoring last component of path because it's a file
	if(!from_dir) diff--;

	struct slice dotdot = str_slice("../");
	struct slice res[diff + 2];
	for(size_t i = 0; i < diff; i++)
		res[i] = dotdot;

	res[diff] = slice2;
	res[diff + 1] = suffix;

	return compile_str(res);
}

char *fs_src_to_dest_path(enum outputs output, char *path, char **local)
{
	struct slice slice = str_slice(path);
	slice_shift(&slice, strlen(gpaths.src_from_cwd));
	slice_shift(&slice, strspn(slice.start, "/"));
	slice_rm_suffix(&slice, DOC_SUFFIX, sizeof(DOC_SUFFIX) - 1);

	if(local != NULL)
		*local = strndup(slice.start, slice.len);

	return compile_str(((struct slice[]){
		str_slice(gpaths.output[output].dest_from_cwd),
		str_slice("/"),
		slice,
		str_slice(output_info[output].file_suffix),
	}));
}

char *fs_path_file_to_dest_path(enum outputs output, char *path)
{
	return compile_str(((struct slice[]){
		str_slice(gpaths.output[output].dest_from_cwd),
		str_slice("/"),
		str_slice(path),
		str_slice(output_info[output].file_suffix),
	}));
}

char *fs_path_to_dest(enum outputs output, char *path)
{
	return compile_str(((struct slice[]){
		str_slice("/"),
		str_slice(path),
		str_slice(output_info[output].suffix),
	}));
}
