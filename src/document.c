#define _POSIX_C_SOURCE 200809L

#include <ctype.h>
#include <limits.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "def.h"
#include "log.h"
#include "main.h"
#include "sliceop.h"
#include "util.h"

#include "document.h"

#define litcmp(str, slice) \
	strncasecmp(str, slice.start, \
			(slice.len<sizeof(str)?slice.len:sizeof(str)))

// TODO: make treatment of invalid syntax as warnings/errors consistent
// throughout the entire file

static const char *nouns[] = {
	[DocBreak] = "a break", [DocCode] = "a code block",
	[DocEmbed] = "an embed link", [DocHeading] = "a heading",
	[DocList] = "a list", [DocOrderedList] = "an ordered list",
	[DocListItem] = "a list item", [DocOrderedItem] = "a list item",
	[DocPath] = "a path", [DocQuote] = "a quote", [DocText] = "text",
	[DocParagraph] = "a paragraph", [DocNone] = "the root document",
};


struct doc_file doc_file_init(void)
{
	return (struct doc_file){
		.vars = array_init(sizeof(struct doc_var)),
		.head = array_init(sizeof(char)),
		.present = NULL,
		.rest = 0,
		.filename = NULL,
	};
}

// Make a copy of src that only includes variables.
struct doc_file doc_file_shallow_copy(struct doc_file *src) {

	struct doc_file res = *src;

	res.vars.data = xmalloc(res.vars.len * res.vars.size);
	memcpy(res.vars.data, src->vars.data, res.vars.len * res.vars.size);
	// Copy over only what's needed of the file contents.
	res.head.data = xmalloc(res.rest);
	memcpy(res.head.data, src->head.data, res.rest);
	res.head.len = res.rest;

	res.present = NULL;
	res.filename = NULL;

	return res;
}

void doc_file_deinit(struct doc_file *doc_file)
{
	free(doc_file->vars.data);
	free(doc_file->head.data);
	free(doc_file->filename);
	free(doc_file->present);
}

// fgetc but it ignores null bytes
static int ogetc(FILE *stream)
{
	int c;

	while((c = fgetc(stream)) == '\0')
		;

	return c;
}

static inline void load_varline(int line, FILE *file, char *filename,
		struct doc_file *doc_file)
{
	int c;
	struct doc_var var;

	// pass through any initial whitespace
	while((c = ogetc(file)) == ' ' || c == '\t')
		;

	// leave if the var line is empty
	if(c == '\n' || c == '\r' || c == EOF)
		return;

	// var name begins one char past the current end of the buffer
	var.name = doc_file->head.len;

	// add the variable name to the buffer, or leave if the name is invalid
	do {
		if(!VAR_CHAR_IS_VALID(c))
			goto invalid;

		*((char *)array_add(&doc_file->head, 1)) = c;
	} while((c = ogetc(file)) != VAR_DOC_NAMEVAL_DELIM);

	// add a character for use as a null byte
	char *name_end = array_add(&doc_file->head, 1);
	size_t name_end_i = doc_file->head.len - 1;
	// test if the variable's length is zero
	if(var.name == name_end_i)
		goto invalid;
	*name_end = '\0';

	// value begins after the name's terminating null byte
	var.val = name_end_i + 1;

	// trim any initial value whitespace
	while((c = ogetc(file)) == ' ' || c == '\t')
		;

	if(c == '\n' || c == '\r' || c == EOF) {
		// value is empty
		*((char *)array_add(&doc_file->head, 1)) = '\0';
		goto end;
	}

	// last non-whitespace char
	size_t last = var.val;
	do {
		*((char *)array_add(&doc_file->head, 1)) = c;
		if(c != ' ' && c != '\t')
			last = doc_file->head.len - 1;
	} while((c = ogetc(file)) != '\n' && c != '\r' && c != EOF);

	// set the buffer length to be just big enough for a null byte
	size_t new_len = ++last + 1;
	array_atleast_cap(&doc_file->head, new_len);
	doc_file->head.len = new_len;

	// add terminating null byte, trimming off any ending whitespace
	doc_file_head(*doc_file)[last] = '\0';

end:
	*((struct doc_var *)array_add(&doc_file->vars, 1)) = var;
	return;

invalid:
	// pass through to the end of the line, if not already there
	for(; c != '\n' && c != '\r' && c != EOF; c = ogetc(file))
		;
	LOG_WARN("%s:%d: Invalid varaible format",
			filename, line);
}

bool doc_file_load(FILE *file, char *filename, struct doc_file *doc_file)
{
	int c;

	doc_file->vars.len = 0;
	doc_file->head.len = 0;
	doc_file->rest = 0;
	doc_file->filename = xstrdup(filename);

	int line;
	for(line = 1; (c = fgetc(file)) == VAR_DOC_BEGIN; line++)
		load_varline(line, file, filename, doc_file);

	doc_file->line = line;
	doc_file->rest = doc_file->head.len;

	while(c != EOF) {
		*((char *)array_add(&doc_file->head, 1)) = c;
		c = fgetc(file);
	}

	*((char *)array_add(&doc_file->head, 1)) = '\0';

	if(ferror(file)) {
		LOG_ERR("Failed to read file %s", filename);
		return false;
	}

	return true;
}

bool doc_match_var(char *match, struct doc_file *doc_file, size_t *where)
{
	for(size_t i = 0; i < doc_file->vars.len; i++) {
		if(!strcasecmp(doc_file_var(*doc_file, i, name), match)) {
			*where = i;
			return true;
		}
	}

	return false;
}

struct doc_iter doc_make_iter_slice(struct slice slice, int line, char *filename,
		char *present)
{
	return (struct doc_iter){
		.buf = slice,
		.pos = 0,
		.linescan = 0,
		.line = line,
		.graph_this_line = false,
		.graph_at = 0,
		.stack_len = 0,
		.stack_add_pending = false,
		.stack_remove_pending = 0,
		.filename = filename,
		.quote_level = 0,
		.present = present,
	};
}

struct doc_iter doc_make_iter(struct doc_file *doc_file)
{
	struct slice slice = {
		.start = doc_file_rest(*doc_file),
		.len = doc_file->head.len - doc_file->rest - 1,
	};

	doc_file->present = xrealloc(doc_file->present, slice.len);

	return doc_make_iter_slice(slice, doc_file->line, doc_file->filename,
			doc_file->present);
}


// should be called either when a iter->graph_this_line and iter->graph_at are
// needed, or when the position in the buffer is at a line that may need to be
// logged at a future point befor the position is changed
static inline void linescan(struct doc_iter *iter)
{
	for(; iter->linescan <= iter->pos; iter->linescan++) {
		if(iter->buf.start[iter->linescan] == '\n') {
			iter->graph_this_line = false;
			iter->line++;
		} else if(!isspace(iter->buf.start[iter->linescan])) {
			if(!iter->graph_this_line)
				iter->graph_at = iter->linescan;
			iter->graph_this_line = true;
		}
	}
}

static bool try_formal_directive_start(struct doc_iter *iter, size_t *pos_ptr,
		enum doc_end *end, struct slice *body)
{
	char *start = iter->buf.start;
	size_t pos = iter->pos;

	if(start[pos] != '[')
		return false;

	size_t instart, inend;
	for(inend = instart = ++pos; start[inend] != ']'; inend++) {
		if(start[inend] == '\n' || inend >= iter->buf.len - 1) {
			return false;
		} else if(start[inend] == '\\') {
			// do character escapes
			// TODO: make directive commands parse correctly with the escapes
			inend++;
			continue;
		}
	}

	for(pos = inend + 1; isspace(start[pos]); pos++)
		if(pos >= iter->buf.len - 1)
			return false;

	if(start[pos] == '{') {
		if(end)
			*end = DocEndStrong;
	} else if(start[pos] == ':') {
		if(end)
			*end = DocEndWeak;
	} else {
		return false;
	}

	// go to the character after the directive
	pos++;

	if(pos_ptr)
		*pos_ptr = pos;

	if(body) {
		*body = (struct slice){
			.start = &start[instart],
			.len = inend - instart,
		};
	}

	return true;
}

// determine how many directives on the stack should be removed before adding a
// new one, which is a text directive if is_text is true
static inline int remove_how_many_before(struct doc_iter *iter,
		bool is_text)
{
	int res = 0;

	for(int spos = iter->stack_len - 1; spos >= 0; spos--) {
		enum doc_end end = iter->stack[spos].end;
		if(end == DocEndStrong ||
				(iter->stack[spos].token.type != DocText && is_text)) {
			break;
		} else {
			res++;
		}
	}

	return res;
}

static enum doc_token_type get_list_item_type(struct doc_iter *iter,
		int rm, struct doc_stack_token *store, bool *invalid)
{
	struct doc_stack_token *parent;

	if(iter->stack_len > rm) {
		parent = &iter->stack[iter->stack_len - rm - 1];
	}  else {
		*invalid = true;
		return DocListItem;
	}

	*invalid = false;
	if(parent->token.type == DocList) {
		return DocListItem;
	} else if(parent->token.type == DocOrderedList) {
		if(store)
			store->token.ordered.number = parent->token.ordered.number++;
		return DocOrderedItem;
	} else {
		*invalid = true;
		return DocListItem;
	}
}

static inline bool try_fancy_directive(struct doc_iter *iter, bool *invalid,
		struct doc_stack_token *store, int *remove_before, size_t *pos_ptr)
{
	linescan(iter);

	if(!iter->graph_this_line || iter->graph_at != iter->pos)
		return false;

	enum doc_token_type prev_notext = DocNone;
	int rm_notext = remove_how_many_before(iter, false);
	if(iter->stack_len > rm_notext)
		prev_notext = iter->stack[iter->stack_len - rm_notext - 1].token.type;
	enum doc_token_type new_type = DocNone;
	size_t pos = iter->pos;

	// a slice starting at the current position in the iter buffer and ending
	// at the last non-whitespace character
	struct slice str;
	str.start = &iter->buf.start[pos];
	// the highest length str is allowed to be
	size_t sublen = iter->buf.len - pos;
	for(str.len = 0; str.len < sublen; str.len++)
		if(isspace(str.start[str.len]))
			break;

	// later code assumes this is not the case
	if(str.len == 0)
		return false;

	// determine if str consists exclusively of '#' characters
	bool is_heading = true;
	for(size_t i = 0; i < str.len; i++) {
		if(str.start[i] != '#') {
			is_heading = false;
			break;
		}
	}

	if(is_heading) {
		pos += str.len;
		new_type = DocHeading;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

		// TODO: do something less bad
		if(store) {
			if(str.len > INT_MAX)
				store->token.heading.level = INT_MAX;
			else
				store->token.heading.level = str.len;
		}

	} else if(str.start[0] == '*' || str.start[0] == '-') {
		pos += 1;
		bool tinvalid;
		new_type = get_list_item_type(iter, rm_notext, store, &tinvalid);
		if(tinvalid)
			goto invalid;

	} else {
		return false;
	}

	if(store) {
		store->token.type = new_type;
		store->end = DocEndWeak;
	}

	if(remove_before)
		*remove_before = rm_notext;

	if(pos_ptr)
		*pos_ptr = pos;

	return true;

invalid:
	if(invalid)
		*invalid = true;

	if(pos_ptr)
		*pos_ptr = pos;

	LOG_ERR("%s:%d: Unexpectedly got %s inside %s", iter->filename, iter->line,
			nouns[new_type], nouns[prev_notext]);

	return false;
}

static void present_slice(struct doc_iter *iter, struct slice *slice)
{
	struct slice old = *slice;
	slice->start = &iter->present[slice->start - iter->buf.start];
	slice->len = 0;
	for(size_t i = 0; i < old.len; i++) {
		if(old.start[i] == '\\')
			continue;
		slice->start[slice->len++] = old.start[i];
	}
}

static struct slice pull_text(struct doc_iter *iter)
{
	size_t start_pos = iter->pos;

	for(; iter->pos < iter->buf.len; iter->pos++) {
		// skip past an escaped character
		if(iter->buf.start[iter->pos] == '\\') {
			// skip the character but make sure we won't put iter->pos past the
			// end of its buffer
			if(iter->pos != iter->buf.len - 1)
				iter->pos++;
			continue;
		}

		if(try_formal_directive_start(iter, NULL, NULL, NULL) ||
				try_fancy_directive(iter, NULL, NULL, NULL, NULL) ||
				iter->buf.start[iter->pos] == '}')
			break;
	}

	struct slice res = {
		.start = &iter->buf.start[start_pos],
		.len = iter->pos - start_pos,
	};

	slice_trim(&res);

	present_slice(iter, &res);

	return res;
}

static bool slicetok_escaped(struct slice *str, struct slice *t)
{
	char *end = &str->start[str->len];
	for(t->start = t->start + t->len; t->start < end; t->start++) {
		if(isspace(*(t->start)))
			continue;

		if(*(t->start) == '\\') {
			t->start++;
			continue;
		}

		for(t->len = 0; &t->start[t->len] < end; t->len++) {
			if(isspace(t->start[t->len]))
				break;

			if(t->start[t->len] == '\\') {
				t->start++;
				if(!(&t->start[t->len] < end))
					break;
				continue;
			}
		}

		return true;
	}
	return false;
}

// uses slicetok to get a set number of arguments, specified by wanted. If
// the number of arguments does not match, the function returns false. str and
// t are the same as the corresponding arguments in strtok.
static inline bool n_args(struct doc_iter *iter, struct slice *err,
		struct slice *str, struct slice *t, int wanted, ...)
{
	char errstr[err->len + 1];
	errstr[sizeof(errstr) - 1] = '\0';
	strncpy(errstr, err->start, sizeof(errstr) - 1);

	va_list ap;

	va_start(ap, wanted);

	int n;
	for(n = 0; n < wanted; n++) {
		struct slice *store = va_arg(ap, struct slice *);
		if(!slicetok_escaped(str, t)) {
			va_end(ap);
			LOG_ERR("%s:%d: Expected %d args for %s, got %d", iter->filename,
					iter->line, wanted, errstr, n);
			return false;
		}

		*store = *t;
	}

	va_end(ap);

	if(slicetok_escaped(str, t)) {
		for(++n; slicetok_escaped(str, t); n++)
			;
		LOG_ERR("%s:%d: Expected %d args for %s, got %d", iter->filename,
				iter->line, wanted, errstr, n);
		return false;
	}

	return true;
}

// returns all the text flags for all the directives on the stack OR'd together
// with the flags passed
static inline enum doc_text_flags full_flags(struct doc_iter *iter,
		enum doc_text_flags flags, int remove)
{
	for(int spos = iter->stack_len - remove - 1; spos >= 0; spos--)
		if(iter->stack[spos].token.type == DocText)
			return flags | iter->stack[spos].token.text.full_flags;

	return flags;
}

static inline bool try_formal_directive(struct doc_iter *iter, bool *invalid,
		struct doc_stack_token *store, int *remove_before)
{
	struct slice body;

	linescan(iter);

	if(!try_formal_directive_start(iter, &iter->pos, &store->end, &body))
		return false;

	struct slice tok = slicetok_init(&body);
	struct slice first_tok;
	if(!slicetok_escaped(&body, &tok)) {
		// TODO: error message
		*invalid = true;
		return false;
	}

	first_tok = tok;
	present_slice(iter, &first_tok);

	// if at a text directive, set the token's text slice
	if(first_tok.start[0] == '^')
		store->token.text.text = pull_text(iter);

	enum doc_token_type prev_notext = DocNone;
	enum doc_token_type prev_text = DocNone;
	int rm_notext = remove_how_many_before(iter, false);
	int rm_text = remove_how_many_before(iter, true);
	enum doc_token_type new_type = DocNone;
	if(iter->stack_len > rm_notext)
		prev_notext = iter->stack[iter->stack_len - rm_notext - 1].token.type;
	if(iter->stack_len > rm_text)
		prev_text = iter->stack[iter->stack_len - rm_text - 1].token.type;

	if(!litcmp("^n", first_tok) ||
			!litcmp("^normal", first_tok)) {
		new_type = DocText;
		if(prev_text != DocEmbed && prev_text != DocText &&
				prev_text != DocParagraph && prev_text != DocPath &&
				prev_text != DocHeading && prev_text != DocListItem &&
				prev_text != DocOrderedItem && prev_text != DocCode)
			goto invalid;

		store->token.text.flags = 0;
	} else if(!litcmp("^b", first_tok) ||
			!litcmp("^bold", first_tok)) {
		new_type = DocText;
		if(prev_text != DocEmbed && prev_text != DocText &&
				prev_text != DocParagraph && prev_text != DocPath &&
				prev_text != DocHeading && prev_text != DocListItem &&
				prev_text != DocOrderedItem && prev_text != DocCode)
			goto invalid;

		store->token.text.flags = DocTextBold;
	} else if(!litcmp("^i", first_tok) ||
			!litcmp("^italic", first_tok)) {
		new_type = DocText;
		if(prev_text != DocEmbed && prev_text != DocText &&
				prev_text != DocParagraph && prev_text != DocPath &&
				prev_text != DocHeading && prev_text != DocListItem &&
				prev_text != DocOrderedItem && prev_text != DocCode)
			goto invalid;

		store->token.text.flags = DocTextItalic;
	} else if(!litcmp("^bi", first_tok) ||
			!litcmp("^bolditalic", first_tok)) {
		new_type = DocText;
		if(prev_text != DocEmbed && prev_text != DocText &&
				prev_text != DocParagraph && prev_text != DocPath &&
				prev_text != DocHeading && prev_text != DocListItem &&
				prev_text != DocOrderedItem && prev_text != DocCode)
			goto invalid;

		store->token.text.flags = DocTextBold | DocTextItalic;
	} else if(!litcmp("^code", first_tok) ||
			!litcmp("^c", first_tok)) {
		new_type = DocText;
		if(prev_text != DocEmbed && prev_text != DocText &&
				prev_text != DocParagraph && prev_text != DocPath &&
				prev_text != DocHeading && prev_text != DocListItem &&
				prev_text != DocOrderedItem && prev_text != DocCode)
			goto invalid;

		store->token.text.flags = DocTextCode;
	} else if(!litcmp("p", first_tok) ||
			!litcmp("paragraph", first_tok)) {
		new_type = DocParagraph;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

	} else if(!litcmp("#", first_tok) ||
			!litcmp("heading", first_tok)) {
		new_type = DocHeading;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

		struct slice level;
		if(!n_args(iter, &first_tok, &body, &tok, 1, &level)) {
			*invalid = true;
			return false;
		}
		present_slice(iter, &level);

		char levelstr[level.len + 1];
		levelstr[sizeof(levelstr) - 1] = '\0';
		strncpy(levelstr, level.start, sizeof(levelstr) - 1);

		char *bad_ptr;
		long levelnum = strtol(levelstr, &bad_ptr, 10);
		if(bad_ptr == levelstr || levelnum < 1 || levelnum > INT_MAX) {
			*invalid = true;
			LOG_ERR("%s:%d: Invalid heading level %s, expected a number"
					" between 1 and %d", iter->filename, iter->line,
					levelstr, INT_MAX);

			return false;
		}
		store->token.heading.level = (int)levelnum;

	} else if(!litcmp("l", first_tok) ||
			!litcmp("list", first_tok)) {
		new_type = DocList;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

	} else if(!litcmp("o", first_tok) ||
			!litcmp("ordered", first_tok)) {
		new_type = DocOrderedList;
		store->token.ordered.number = 1;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

	} else if(!litcmp("item", first_tok) ||
			!litcmp("*", first_tok)) {
		bool tinvalid;
		new_type = get_list_item_type(iter, rm_notext, store, &tinvalid);
		if(tinvalid)
			goto invalid;

	} else if(!litcmp("=>", first_tok) ||
			!litcmp("link", first_tok)) {
		new_type = DocPath;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

		if(!n_args(iter, &first_tok, &body, &tok, 1, &store->token.path.path)) {
			*invalid = true;
			return false;
		}
		present_slice(iter, &store->token.path.path);

	} else if(!litcmp(">>", first_tok) ||
			!litcmp("embed", first_tok)) {
		new_type = DocEmbed;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

		struct slice mimetype;
		if(!n_args(iter, &first_tok, &body, &tok, 2, &mimetype, &store->token.embed.path)) {
			*invalid = true;
			return false;
		}
		present_slice(iter, &store->token.embed.path);
		present_slice(iter, &mimetype);

		store->token.embed.mime_type = mimetype;
		// A subtype with an empty length means no subtype.
		store->token.embed.mime_subtype = (struct slice){
			.start = 0,
			.len = 0,
		};
		for(size_t i = 0; i < mimetype.len; i++) {
			if(mimetype.start[i] == '/') {
				// This looks like a well-formed mime type, so set the main
				// mimetype to end before the slash.
				store->token.embed.mime_type.len = i;

				struct slice subtype = {
					.start = &mimetype.start[i],
					.len = mimetype.len - i,
				};
				// An asterisk as the subtype matches any type, so only store
				// the subtype if it isn't one.
				if(strncmp(subtype.start, "*", subtype.len) != 0)
					store->token.embed.mime_subtype = subtype;

				break;
			}
		}

	} else if(!litcmp("c", first_tok) ||
			!litcmp("code", first_tok)) {
		new_type = DocCode;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

	} else if(!litcmp("q", first_tok) ||
			!litcmp("quote", first_tok)) {
		new_type = DocQuote;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;

	} else if(!litcmp("b", first_tok) ||
			!litcmp("break", first_tok)) {
		new_type = DocBreak;
		if(prev_notext != DocNone && prev_notext != DocQuote)
			goto invalid;
	} else {
		char tokarr[first_tok.len + 1];
		tokarr[sizeof(tokarr) - 1] = '\0';
		strncpy(tokarr, first_tok.start, sizeof(tokarr) - 1);

		*invalid = true;
		LOG_ERR("%s:%d: Unknown directive %s", iter->filename, iter->line,
				tokarr);
		return false;
	}

	// should only happen if n_args was not called before
	if(first_tok.start == tok.start) {
		if(!n_args(iter, &first_tok, &body, &tok, 0)) {
			*invalid = true;
			// TODO: error message
			return false; // had more than zero arguments
		}
	}

	store->token.type = new_type;
	if(new_type == DocText) {
		*remove_before = rm_text;
		store->token.text.full_flags = full_flags(iter,
				store->token.text.flags, rm_text);
	} else {
		*remove_before = rm_notext;
	}

	return true;

invalid:
	*invalid = true;
	char tokarr[first_tok.len + 1];
	tokarr[sizeof(tokarr) - 1] = '\0';
	strncpy(tokarr, first_tok.start, sizeof(tokarr) - 1);

	LOG_ERR("%s:%d: Unexpectedly got %s inside %s", iter->filename,
			iter->line, tokarr,
			nouns[(new_type == DocText)?prev_text:prev_notext]);

	return false;
}

static inline bool try_directive(struct doc_iter *iter, bool *invalid,
		struct doc_stack_token *store, int *remove_before)
{
	*invalid = false;

	if(try_formal_directive(iter, invalid, store, remove_before)) {
		return true;
	} else {
		if(*invalid)
			return false;
		return try_fancy_directive(iter, invalid, store, remove_before,
				&iter->pos);
	}
}

static inline int remove_how_many_explicit(struct doc_iter *iter)
{
	int res = 0;

	for(int spos = iter->stack_len - 1; spos >= 0; spos--) {
		enum doc_end end = iter->stack[spos].end;

		res++;

		if(end == DocEndStrong)
			return res;
	}

	linescan(iter);
	LOG_ERR("%s:%d: Unexpeted closing bracket at root document",
			iter->filename, iter->line);

	return -1;
}

static inline struct doc_stack_token *pop_stack_pending(struct doc_iter *iter)
{
	if(iter->stack_remove_pending > 0) {
		iter->stack_remove_pending--;
		struct doc_stack_token *pop = &iter->stack[--iter->stack_len];
		if(pop->token.type == DocQuote)
			iter->quote_level--;
		return pop;
	} else {
		return NULL;
	}
}

static inline struct doc_stack_token *push_stack(struct doc_iter *iter,
		struct doc_stack_token new)
{
	// TODO: handle buffer overflow
	struct doc_stack_token *tail = &iter->stack[iter->stack_len++];
	*tail = new;
	if(tail->token.type == DocQuote)
		iter->quote_level++;
	return tail;
}

int doc_iter_walk(struct doc_iter *iter, struct doc_stack_token **tok_ptr,
		bool *is_reused)
{
	char *start = iter->buf.start;
	*is_reused = false;

	struct doc_stack_token *pop = pop_stack_pending(iter);
	if(pop) {
		*tok_ptr = pop;

		return -1;
	} else if(iter->stack_add_pending) {
		*tok_ptr = push_stack(iter, iter->pending);
		iter->stack_add_pending = false;

		return 1;
	}

	for(; iter->pos < iter->buf.len; iter->pos++) {
		bool invalid;
		struct doc_stack_token new_tok;

		// don't do special processing for backslashes and their following
		// characters by treating them as normal text
		if(start[iter->pos] == '\\')
			goto text;

		if(try_directive(iter, &invalid, &new_tok,
					&iter->stack_remove_pending)) {
			struct doc_stack_token *pop = pop_stack_pending(iter);
			if(pop) {
				iter->pending = new_tok;
				iter->stack_add_pending = true;
				*tok_ptr = pop;

				return -1;
			} else {
				*tok_ptr = push_stack(iter, new_tok);

				return 1;
			}
		}

		if(invalid)
			return 0;

		if(start[iter->pos] == '}') {
			if((iter->stack_remove_pending =
						remove_how_many_explicit(iter)) <= 0) {
				return 0;
			}
			iter->pos++;

			struct doc_stack_token *pop = pop_stack_pending(iter);
			*tok_ptr = pop;

			return -1;
		}

		if(!isspace(start[iter->pos])) {
text:
			linescan(iter);
			struct slice text = pull_text(iter);
			(void)text;

			enum doc_token_type prev_type = DocNone;
			if(iter->stack_len)
				prev_type = iter->stack[iter->stack_len - 1].token.type;

			if(prev_type == DocText) {
				struct doc_stack_token *old_ptr = &iter->stack[iter->stack_len - 1];
				*tok_ptr = old_ptr;
				*is_reused = true;
				old_ptr->token.text.text = text;
			} else {
				// TODO: clean this up
				if(prev_type != DocEmbed && prev_type != DocText &&
						prev_type != DocParagraph && prev_type != DocPath &&
						prev_type != DocHeading && prev_type != DocListItem &&
						prev_type != DocOrderedItem && prev_type != DocCode) {

					LOG_ERR("%s:%d: Unexpectedly got %s inside %s",
							iter->filename, iter->line, nouns[DocText],
							nouns[prev_type]);

					return 0;
				}

				struct doc_stack_token new_tok;
				new_tok.end = DocEndWeak;
				new_tok.token.type = DocText;
				new_tok.token.text.text = text;
				new_tok.token.text.flags = 0;
				*tok_ptr = push_stack(iter, new_tok);
			}

			return 1;
		}
	}

	// end of file

	// TODO: print an error if any of these should have an explicit end
	if(iter->stack_len) {
		iter->stack_remove_pending = iter->stack_len;
		struct doc_stack_token *pop = pop_stack_pending(iter);

		*tok_ptr = pop;

		return -1;
	}

	return 0;
}
