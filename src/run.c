#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "fs.h"
#include "interface.h"
#include "log.h"
#include "document.h"
#include "target/common.h"
#include "util.h"

#include "run.h"

static struct doc_file doc_file;

static bool run_for(char *path)
{
	struct array filebuf = array_init(sizeof(char));
	FILE *file = fopen(path, "r");
	if(ferror(file)) {
		LOG_ERRNO("Failed to open file %s", path);
		return false;
	}

	if(!doc_file_load(file, path, &doc_file))
		goto error;

	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;
		filebuf.len = 0;
		char *body;

		if(!output_info[i].func_process(path, &doc_file, &filebuf))
			goto error;

		body = filebuf.data;

		char *local_path;
		char *dest_path = fs_src_to_dest_path(i, path, &local_path);

		struct file_meta meta;
		if(!file_meta_init(&meta, path, dest_path, local_path, body, doc_file))
			goto error;

		for(enum targets j = 0; j < TargetLast; j++) {
			if(!used_targets[j])
				continue;
			if(!target_info[j].func_process(i, &meta))
				continue;
		}

		free(dest_path);
		free(local_path);
	}

	fclose(file);
	free(filebuf.data);
	return true;

error:
	fclose(file);
	free(filebuf.data);
	return false;
}

static inline bool init_targets(void)
{
	for(enum targets i = 0; i < TargetLast; i++)
		if(used_targets[i])
			if(!target_info[i].func_init())
				return false;
	return true;
}

static inline void deinit_targets(void)
{
	for(enum targets i = 0; i < TargetLast; i++)
		if(used_targets[i])
			target_info[i].func_deinit();
}

static inline void deinit_targets_err(void)
{
	for(enum targets i = 0; i < TargetLast; i++)
		if(used_targets[i])
			target_info[i].func_deinit_err();
}

bool run(void)
{
	init_targets();

	doc_file = doc_file_init();
	if(!fs_walk_source(run_for))
		goto fail;

	doc_file_deinit(&doc_file);
	deinit_targets();
	return true;
fail:
	doc_file_deinit(&doc_file);
	deinit_targets_err();
	return false;
}
