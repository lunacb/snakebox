#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "fs.h"
#include "interface.h"
#include "log.h"
#include "main.h"
#include "document.h"
#include "target/common.h"
#include "template.h"
#include "util.h"

#include "target/target.h"

static struct array docs[OutputLast]; // struct file_meta

static void deinit() {
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		for(size_t j = 0; j < docs[i].len; j++) {
			struct file_meta *meta = &((struct file_meta *)docs[i].data)[j];
			file_meta_deinit(meta);
		}

		free(docs[i].data);
	}
}

bool target_index_init(void)
{
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		docs[i] = array_init(sizeof(struct file_meta));
	}
	return true;
}

bool target_index_process(enum outputs output, struct file_meta *meta)
{
	// Collect docs into an array.
	*((struct file_meta *)array_add(&docs[output], 1)) = file_meta_clone(meta);
	return true;
}

void target_index_deinit(void)
{
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		struct var_stack stack = var_stack_init();
		var_stack_set_current_path_file(&stack, PathFileIndex, false);
		var_stack_push_value(&stack, "prefix", prefix[i]?prefix[i]:"");

		// Sort docs by modification date.
		qsort(docs[i].data, docs[i].len, sizeof(struct file_meta), meta_compar);

		// Make the index file.

		char *index_path = fs_path_file_to_dest_path(i, path_files[PathFileIndex]);

		if(!fs_make_basedir(index_path)) {
			LOG_ERR("Failed to create destination file %s", index_path);
			goto fail;
		}

		FILE *file = fopen(index_path, "w");
		if(file == NULL) {
			LOG_ERR("Failed to open destination file %s", index_path);
			fclose(file);
			goto fail;
		}
		free(index_path);

		// Write to the index file from the template.

		struct template_ctx *ctx = &template_map[i][TemplateIndex];
		struct template_walker walker = template_walker_init(ctx);

		struct template_segment *seg;
		char *val;

		while((seg = template_walk(&walker)) != NULL)  {
			switch(seg->type) {
			case TemplateText:
				fwrite(seg->text.start, 1, seg->text.len, file);

				break;

			case TemplateVar:
				if((val = var_stack_match(i, &stack, seg->var.name)) != NULL) {
					fputs(val, file);
					var_stack_cleanup(&stack);
				} else {
					LOG_WARN("Unkown variable %s used in template %s",
							seg->var.name, gpaths.output[i].templates[TemplateIndex]);
				}

				break;

			case TemplateArray:
				if(!strcasecmp(seg->array.name, "preview")) {
					struct template_walker iwalker =
						template_walker_init_array(ctx, seg);

					for(size_t j = 0; j < docs[i].len; j++) {
						struct file_meta *meta = &((struct file_meta *)docs[i].data)[j];

						if(!write_document_from_template(i, TemplateIndex,
								meta, &stack, &iwalker, ctx, file)) {
							fclose(file);
							goto fail;
						}
					}
				} else {
					LOG_WARN("Nonexistent list %s used in template %s", seg->array.name,
							gpaths.output[i].templates[TemplateIndex]);
				}
			}
		}

		if(ferror(file)) {
			index_path = fs_path_file_to_dest_path(i, path_files[PathFileIndex]);
			LOG_ERR("Failed to write to file %s", index_path);
			free(index_path);
			fclose(file);
			goto fail;
		}

		fclose(file);
	}

fail:
	deinit();
}

void target_index_deinit_err(void)
{
	deinit();
}
