#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "document.h"
#include "fs.h"
#include "interface.h"
#include "log.h"
#include "main.h"
#include "sliceop.h"
#include "target/common.h"
#include "template.h"
#include "util.h"

#include "target/target.h"

struct tag {
	char *name;
	char *path;
	struct array docs; // struct file_meta *
};

static struct array docs[OutputLast]; // struct file_meta

static void deinit() {
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		for(size_t j = 0; j < docs[i].len; j++) {
			struct file_meta *meta = &((struct file_meta *)docs[i].data)[j];
			file_meta_deinit(meta);
		}

		free(docs[i].data);
	}
}

bool target_tag_index_init(void)
{
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		docs[i] = array_init(sizeof(struct file_meta));
	}
	return true;
}

bool target_tag_index_process(enum outputs output, struct file_meta *meta)
{
	// Collect docs into an array.
	*((struct file_meta *)array_add(&docs[output], 1)) = file_meta_clone(meta);

	return true;
}

int tag_compar(const void *first, const void *second)
{
	const struct tag *t1 = first, *t2 = second;
	return strcmp(t1->name, t2->name);
}

// TODO: ensure that all template lists have the same available variables as
// their containing scope
void target_tag_index_deinit(void)
{
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		// Sort docs by modification date.
		qsort(docs[i].data, docs[i].len, sizeof(struct file_meta), meta_compar);

		// Index docs based on tags.

		struct array tags = array_init(sizeof(struct tag));

		for(size_t j = 0; j < docs[i].len; j++) {
			struct file_meta *meta = &((struct file_meta *)docs[i].data)[j];
			size_t doc_var_i;

			if(doc_match_var("tags", &meta->doc_file, &doc_var_i)) {
				char *var_ptr = doc_file_var(meta->doc_file, doc_var_i, val);
				char *str = xstrdup(var_ptr);

				char *tok = strtok(str, ", \t");

				for(; tok != NULL; tok = strtok(NULL, ", \t")) {
					bool found = false;
					for(size_t j = 0; j < tags.len; j++) {
						struct tag *tag = &((struct tag *)tags.data)[j];
						if(!strcmp(tag->name, tok)) {
							*((struct file_meta **)array_add(&tag->docs, 1)) = meta;
							found = true;
							break;
						}
					}

					if(!found) {
						struct tag new = {
							.name = xstrdup(tok),
							.path = compile_str(((struct slice[]){
								str_slice(path_files[PathFileTagDir]),
								str_slice("/"),
								str_slice(tok),
							})),
							.docs = array_init(sizeof(struct file_meta *)),
						};
						*((struct file_meta **)array_add(&new.docs, 1)) = meta;
						*((struct tag *)array_add(&tags, 1)) = new;
					}
				}

				free(str);
			}
		}

		// Sort tags alphabetically.
		qsort(tags.data, tags.len, sizeof(struct tag), tag_compar);

		// Make the tag index file.

		char *index_path = fs_path_file_to_dest_path(i, path_files[PathFileTagIndex]);

		if(!fs_make_basedir(index_path)) {
			LOG_ERR("Failed to create destination file %s", index_path);
			goto fail;
		}

		FILE *file = fopen(index_path, "w");
		if(file == NULL) {
			LOG_ERR("Failed to open destination file %s", index_path);
			fclose(file);
			goto fail;
		}

		free(index_path);

		// Write to the index file from the template.

		struct template_ctx *ctx = &template_map[i][TemplateTagIndex];
		struct template_walker walker = template_walker_init(ctx);

		struct var_stack stack = var_stack_init();
		var_stack_set_current_path_file(&stack, PathFileTagIndex, false);
		var_stack_push_value(&stack, "prefix", prefix[i]?prefix[i]:"");

		struct template_segment *seg;
		char *val;

		while((seg = template_walk(&walker)) != NULL)  {
			switch(seg->type) {
				case TemplateText:
					fwrite(seg->text.start, 1, seg->text.len, file);

					break;

				case TemplateVar:
					if((val = var_stack_match(i, &stack, seg->var.name)) != NULL) {
						fputs(val, file);
						var_stack_cleanup(&stack);
					} else {
						LOG_WARN("Unkown variable %s used in template %s",
								seg->var.name, gpaths.output[i].templates[TemplateTagIndex]);
					}

					break;

				case TemplateArray:
					if(!strcasecmp(seg->array.name, "preview")) {
						struct template_walker iwalker =
							template_walker_init_array(ctx, seg);

						for(size_t j = 0; j < tags.len; j++) {
							struct tag *tag = &((struct tag *)tags.data)[j];

							struct var_stack_pointer pointer = var_stack_get_pointer(&stack);
							var_stack_push_path_file(&stack, PathFileSingleTag, tag->path);
							var_stack_push_value(&stack, "tag", tag->name);

							while((seg = template_walk(&iwalker)) != NULL) {
								switch(seg->type) {
									case TemplateText:
										fwrite(seg->text.start, 1, seg->text.len, file);

										break;

									case TemplateVar:
										if((val = var_stack_match(i,
												&stack, seg->var.name)) != NULL) {
											fputs(val, file);
											var_stack_cleanup(&stack);
										} else {
											LOG_WARN("Unkown variable %s used in template %s",
													seg->var.name,
													gpaths.output[i].templates[TemplateTagIndex]);
										}

										break;

									case TemplateArray:
										LOG_WARN("Unkown array %s used in template %s",
												seg->array.name,
												gpaths.output[i].templates[TemplateTagIndex]);

										break;
								}
							}

							var_stack_pop(&stack, &pointer);
						}
					} else {
						LOG_WARN("Nonexistent list %s used in template %s", seg->array.name,
								gpaths.output[i].templates[TemplateTagIndex]);
					}
			}
		}

		if(ferror(file)) {
			LOG_ERR("Failed to write to file %s", index_path);
			fclose(file);
			goto fail;
		}

		fclose(file);

		for(size_t j = 0; j < tags.len; j++) {
			struct tag *tag = &((struct tag *)tags.data)[j];

			// Make the tag file.

			char *tag_path = fs_path_file_to_dest_path(i, tag->path);

			if(!fs_make_basedir(tag_path)) {
				LOG_ERR("Failed to create destination file %s", tag_path);
				fclose(file);
				goto fail;
			}

			FILE *tag_file = fopen(tag_path, "w");
			if(tag_file == NULL) {
				LOG_ERR("Failed to open destination file %s", tag_path);
				fclose(file);
				goto fail;
			}

			free(tag_path);

			// Write to the file from the template.

			struct template_ctx *ctx = &template_map[i][TemplateTagSingleIndex];
			struct template_walker walker = template_walker_init(ctx);

			struct var_stack stack = var_stack_init();
			var_stack_push_path_file(&stack, PathFileSingleTag, tag->path);
			var_stack_set_current_path_file(&stack, PathFileSingleTag, false);
			var_stack_push_value(&stack, "prefix", prefix[i]?prefix[i]:"");
			var_stack_push_value(&stack, "tag", tag->name);

			struct template_segment *seg;
			char *val;

			while((seg = template_walk(&walker)) != NULL)  {
				switch(seg->type) {
				case TemplateText:
					fwrite(seg->text.start, 1, seg->text.len, tag_file);

					break;

				case TemplateVar:
					if((val = var_stack_match(i, &stack, seg->var.name))
							!= NULL) {
						fputs(val, tag_file);
						var_stack_cleanup(&stack);
					} else {
						LOG_WARN("Unkown variable %s used in template %s",
								seg->var.name,
								gpaths.output[i].templates[TemplateTagSingleIndex]);
					}

					break;

				case TemplateArray:
					if(!strcasecmp(seg->array.name, "preview")) {
						// TODO: make a common function for this
						struct template_walker iwalker =
							template_walker_init_array(ctx, seg);

						for(size_t j = 0; j < tag->docs.len; j++) {
							struct file_meta *meta = ((struct file_meta **)tag->docs.data)[j];

							if(!write_document_from_template(i, TemplateTagSingleIndex,
										meta, &stack, &iwalker, ctx, tag_file)) {
								fclose(tag_file);
								goto fail;
							}
						}
					} else {
						LOG_WARN("Nonexistent list %s used in template %s", seg->array.name,
								gpaths.output[i].templates[TemplateTagSingleIndex]);
					}
				}
			}
			if(ferror(tag_file)) {
				char *tag_path = fs_path_file_to_dest_path(i, tag->path);
				LOG_ERR("Failed to write to file %s", tag_path);
				free(tag_path);
				fclose(tag_file);
				goto fail;
			}

			fclose(tag_file);
		}
	}

	deinit();
	return;

fail:
	deinit();
}

void target_tag_index_deinit_err(void)
{
	deinit();
}
