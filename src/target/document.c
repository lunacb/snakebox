#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "fs.h"
#include "interface.h"
#include "log.h"
#include "main.h"
#include "document.h"
#include "target/common.h"
#include "template.h"
#include "util.h"

#include "target/target.h"

bool target_document_init(void)
{
	return true;
}

void target_document_deinit(void)
{

}

void target_document_deinit_err(void)
{

}

bool target_document_process(enum outputs output, struct file_meta *meta)
{
	if(!fs_make_basedir(meta->dest_path)) {
		LOG_ERR("Failed to create destination file %s", meta->dest_path);
		return false;
	}
	FILE *file = fopen(meta->dest_path, "w");
	if(file  == NULL) {
		LOG_ERRNO("Cannot open file %s", meta->dest_path);
		fclose(file);
		return false;
	}

	struct template_ctx *ctx = &template_map[output][TemplateDoc];
	struct template_walker walker = template_walker_init(ctx);

	struct var_stack stack = var_stack_init();
	var_stack_push_path_file(&stack, PathFileDoc, meta->local_path);
	var_stack_set_current_path_file(&stack, PathFileDoc, false);
	var_stack_push_value(&stack, "prefix", prefix[output]?prefix[output]:"");

	if(!write_document_from_template(output, TemplateDoc, meta, &stack,
			&walker, ctx, file)) {
		fclose(file);
		return false;
	}

	fclose(file);
	return true;
}
