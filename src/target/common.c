#define _XOPEN_SOURCE
#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <time.h>
#include "document.h"
#include "fs.h"
#include "interface.h"
#include "log.h"
#include "main.h"
#include "sliceop.h"
#include "template.h"
#include "util.h"

#include "target/common.h"

static bool try_time_var(struct doc_file *doc_file, char *name,
		struct time_info *dest)
{
	size_t where;
	if(doc_match_var(name, doc_file, &where)) {
		struct tm var_tm;
		char *tstr = doc_file_var(*doc_file, where, val);
		if(strptime(tstr, date_read_fmt, &var_tm) == NULL) {
			LOG_ERRNO("Failed to parse time string %s", tstr);
			return false;
		}

		dest->tm = var_tm;
		dest->exact = (struct timespec){
			.tv_sec = mktime(&var_tm),
			.tv_nsec = 0,
		};

		return true;
	}

	return false;
}

static bool try_time_stat(struct doc_file *doc_file, struct time_info *dest)
{
	struct stat sb;

	if(stat(doc_file->filename, &sb) == -1) {
		LOG_ERRNO("Failed to stat file %s", doc_file->filename);
		return false;
	}
	dest->exact = sb.st_mtim;

	struct tm tm;

	if(localtime_r(&sb.st_mtim.tv_sec, &tm) == NULL) {
		LOG_ERRNO("Failed to format time");
		return false;
	}
	dest->tm = tm;

	return true;
}

bool file_meta_init(struct file_meta *meta, char *src_path, char *dest_path,
		char *local_path, char *body, struct doc_file doc_file)
{
	if(!try_time_var(&doc_file, "created", &meta->created))
		if(!try_time_stat(&doc_file, &meta->created))
			return false;

	if(!try_time_var(&doc_file, "modified", &meta->modified))
		meta->modified = meta->created;

	if(strftime(meta->created.str, sizeof(meta->created.str),
			date_write_fmt, &meta->created.tm) == 0) {
		LOG_ERRNO("Failed to format time");
		return false;
	}

	if(strftime(meta->modified.str, sizeof(meta->modified.str),
			date_write_fmt, &meta->modified.tm) == 0) {
		LOG_ERRNO("Failed to format time");
		return false;
	}

	meta->src_path = src_path;
	meta->dest_path = dest_path;
	meta->local_path = local_path;
	meta->doc_file = doc_file;
	meta->body = body;

	return true;
}

struct file_meta file_meta_clone(struct file_meta *src)
{
	struct file_meta res = *src;
	res.src_path = xstrdup(res.src_path);
	res.dest_path = xstrdup(res.dest_path);
	res.local_path = xstrdup(res.local_path);
	res.body = xstrdup(res.body);
	res.doc_file = doc_file_shallow_copy(&res.doc_file);

	return res;
}

void file_meta_deinit(struct file_meta *meta)
{
	free(meta->src_path);
	free(meta->dest_path);
	free(meta->local_path);
	doc_file_deinit(&meta->doc_file);
	free(meta->body);
}

int meta_compar(const void *first, const void *second)
{
	struct timespec t1 = ((struct file_meta *)first)->created.exact;
	struct timespec t2 = ((struct file_meta *)second)->created.exact;

	if(t1.tv_sec == t2.tv_sec) {
		if(t1.tv_nsec == t2.tv_nsec) {
			return 0;
		} else {
			return (t1.tv_nsec < t2.tv_nsec);
		}
	} else {
		return (t1.tv_sec < t2.tv_sec);
	}
}

// TODO: remove this function
char *match_path(enum outputs output, char *name, enum path_files src,
		bool from_dir)
{
	(void)name;
	(void)src;
	for(enum path_files i = 0; i < PathFileLast; i++) {
		if(path_varnames[i].absolute != NULL &&
				!strcasecmp(name, path_varnames[i].absolute)) {
			if(path_files[i] != NULL)
				return fs_path_to_dest(output, path_files[i]);
		} else if(path_varnames[i].relative != NULL &&
				!strcasecmp(name, path_varnames[i].relative)) {
			if(path_files[i] != NULL)
				return fs_doc_gen_relative_path(output, path_files[src],
					path_files[i], from_dir);
		}
	}
	return NULL;
}

char *var_stack_match(enum outputs output, struct var_stack *stack, char *name)
{
	for(int i = stack->len - 1; i >= 0; i--) {
		struct var_stack_item *item = &stack->items[i];
		switch(item->type) {
		case VarStackValue:
			if(!strcasecmp(name, item->value.key))
				return item->value.value;
			break;

		case VarStackDoc:;
			size_t doc_var_i;
			if(doc_match_var(name, &item->doc.meta->doc_file, &doc_var_i))
				return doc_file_var(item->doc.meta->doc_file, doc_var_i, val);
			break;

		case VarStackPathFile:;
			// Handled later.
		}
	}

	if(stack->path_file != PathFileLast) {
		char *local_path_files[PathFileLast];
		memcpy(local_path_files, path_files, sizeof(path_files));

		for(int i = 0; i < stack->len; i++) {
			struct var_stack_item *item = &stack->items[i];
			if(item->type == VarStackPathFile)
				local_path_files[item->path_file.file] = item->path_file.value;
		}

		for(enum path_files j = 0; j < PathFileLast; j++) {
			if(path_varnames[j].absolute != NULL &&
					!strcasecmp(name, path_varnames[j].absolute)) {
				if(local_path_files[j] != NULL) {
					stack->to_free = fs_path_to_dest(output, local_path_files[j]);
					return stack->to_free;
				}
			} else if(path_varnames[j].relative != NULL &&
					!strcasecmp(name, path_varnames[j].relative)) {
				if(local_path_files[j] != NULL) {
					stack->to_free = fs_doc_gen_relative_path(output,
						local_path_files[stack->path_file],
						local_path_files[j], stack->is_dir);
					return stack->to_free;
				}
			}
		}
	}

	return NULL;
}

void var_stack_cleanup(struct var_stack *stack)
{
	free(stack->to_free);
	stack->to_free = NULL;
}

bool write_document_from_template(enum outputs output, enum templates template,
		struct file_meta *meta, struct var_stack *stack,
		struct template_walker *walker, struct template_ctx *ctx, FILE *file)
{
	struct template_segment *seg;
	char *val;

	struct var_stack_pointer pointer = var_stack_get_pointer(stack);
	var_stack_push_doc(stack, meta);
	var_stack_push_path_file(stack, PathFileDoc, meta->local_path);
	var_stack_push_value(stack, "body", meta->body);
	var_stack_push_value(stack, "created", meta->created.str);
	var_stack_push_value(stack, "modified", meta->modified.str);

	while((seg = template_walk(walker)) != NULL) {
		switch(seg->type) {
		case TemplateText:
			fwrite(seg->text.start, 1, seg->text.len, file);

			break;

		case TemplateVar:
			if((val = var_stack_match(output, stack, seg->var.name)) != NULL) {
				fputs(val, file);
				var_stack_cleanup(stack);
			} else {
				LOG_WARN("Variable %s used in template %s but not set in file %s",
						seg->var.name, gpaths.output[output].templates[template],
						meta->src_path);
			}

			break;

		case TemplateArray:
			if(!strcasecmp(seg->array.name, "tags")) {
				struct template_walker iwalker =
					template_walker_init_array(ctx, seg);

				size_t doc_var_i;
				if(doc_match_var("tags", &meta->doc_file, &doc_var_i)) {
					char *var_ptr = doc_file_var(meta->doc_file, doc_var_i, val);
					char *str = xstrdup(var_ptr);

					char *tok = strtok(str, ", \t");

					for(; tok != NULL; tok = strtok(NULL, ", \t")) {
							char *path = compile_str(((struct slice[]){
								str_slice(path_files[PathFileTagDir]),
								str_slice("/"),
								str_slice(tok),
							}));

							struct var_stack_pointer tpointer = var_stack_get_pointer(stack);
							var_stack_push_value(stack, "tag", tok);
							var_stack_push_path_file(stack, PathFileSingleTag, path);

							while((seg = template_walk(&iwalker)) != NULL) {
								switch(seg->type) {
									case TemplateText:
										fwrite(seg->text.start, 1, seg->text.len, file);

										break;

									case TemplateVar:
										if((val = var_stack_match(output,
												stack, seg->var.name)) != NULL) {
											fputs(val, file);
											var_stack_cleanup(stack);
										} else {
											LOG_WARN("Variable %s used in template %s but not set in file %s",
													seg->var.name, gpaths.output[output].templates[template],
													meta->src_path);
										}

										break;

									case TemplateArray:
										LOG_WARN("Unkown array %s used in template %s", seg->array.name,
												gpaths.output[output].templates[template]);
										break;
								}
							}

							var_stack_pop(stack, &tpointer);

							free(path);
					}

					free(str);
				}
			} else {
				LOG_WARN("Unkown array %s used in template %s", seg->array.name,
						gpaths.output[output].templates[template]);
			}
			break;
		}

		if(ferror(file)) {
			LOG_ERR("Failed to write to file %s", meta->dest_path);
			var_stack_pop(stack, &pointer);
			return false;
		}
	}

	var_stack_pop(stack, &pointer);

	return true;
}
