#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "document.h"
#include "output/common.h"
#include "sliceop.h"
#include "util.h"

#include "output/output.h"

static void do_smart_quote(struct array *buf, struct doc_iter *iter)
{
	if(iter->quote_level) {
		for(int i = 0; i < iter->quote_level; i++)
			array_add_lit(buf, ">");
		array_add_lit(buf, " ");
	}
}

// blankline being true assumes that the last added character was a newline
static void do_bit(struct array *buf, struct doc_iter *iter,
		char *add, char *rm, int direction, bool blankline)
{
	if(direction == 1) {
		do_smart_quote(buf, iter);
		array_add_str(buf, add, strlen(add));
	} else {
		array_add_str(buf, rm, strlen(rm));
		if(blankline) {
			do_smart_quote(buf, iter);
			array_add_lit(buf, "\n");
		}
	}
}

// blankline being true assumes that the last added character was a newline
static void do_bit_add(struct array *buf, struct doc_iter *iter,
		char *bit, int direction, bool blankline)
{
	if(direction == 1) {
		do_smart_quote(buf, iter);
		array_add_str(buf, bit, strlen(bit));
		if(blankline) {
			do_smart_quote(buf, iter);
			array_add_lit(buf, "\n");
			do_smart_quote(buf, iter);
		}
	}
}

static void do_path(struct array *buf, struct doc_iter *iter, struct slice tok_path,
		int direction)
{
	if(direction == 1) {
		char *path = resolve_path(tok_path, OutputGemini, NULL, NULL, true);

		do_smart_quote(buf, iter);
		array_add_lit(buf, "=> ");
		array_add_str(buf, path, strlen(path));
		array_add_lit(buf, "\t");

		free(path);
	} else {
		array_add_lit(buf, "\n");
		do_smart_quote(buf, iter);
		array_add_lit(buf, "\n");
	}
}

static void do_heading(struct array *buf, struct doc_iter *iter,
		struct doc_stack_token *tok, int direction)
{
	if(direction == 1) {
		do_smart_quote(buf, iter);
		for(int i = 0; i < tok->token.heading.level; i++)
			array_add_lit(buf, "#");
		array_add_lit(buf, " ");
	} else {
		array_add_lit(buf, "\n");
		do_smart_quote(buf, iter);
		array_add_lit(buf, "\n");
	}
}

static void do_text(struct array *buf, struct doc_stack_token *tok,
		bool is_reused, int direction, bool literal)
{
	struct slice *text = &tok->token.text.text;
	struct slice stok = slicetok_init(text);

	if(direction == 1) {
		if(!is_reused) {
			if(tok->token.text.flags & DocTextBold)
				array_add_lit(buf, "*");
			if(tok->token.text.flags & DocTextItalic)
				array_add_lit(buf, "*");
			if(tok->token.text.flags & DocTextCode)
				array_add_lit(buf, "`");
		}

		if(literal) {
			array_add_str(buf, text->start, text->len);
		} else {
			while(slicetok(text, &stok)) {
				array_add_str(buf, stok.start, stok.len);
				array_add_lit(buf, " ");
			}
		}
	} else {
		if(tok->token.text.flags) {
			buf->len--;
			if(tok->token.text.flags & DocTextCode)
				array_add_lit(buf, "`");
			if(tok->token.text.flags & DocTextItalic)
				array_add_lit(buf, "*");
			if(tok->token.text.flags & DocTextBold)
				array_add_lit(buf, "*");
			array_add_lit(buf, " ");
		}
	}
}

static void do_quote(struct array *buf, struct doc_iter *iter, int direction,
		bool *literal)
{
	if(direction == 1) {
		*literal = true;
		do_smart_quote(buf, iter);
		array_add_lit(buf, "```\n");
	} else {
		*literal = false;
		array_add_lit(buf, "\n");
		do_smart_quote(buf, iter);
		array_add_lit(buf, "\n");
		do_smart_quote(buf, iter);
		array_add_lit(buf, "```\n");
	}
}

static void do_list(struct array *buf, struct doc_iter *iter, int direction)
{
	if(direction == -1) {
		array_add_lit(buf, "\n");
		do_smart_quote(buf, iter);
		array_add_lit(buf, "\n");
	}
}

bool output_gemini_process(char *path, struct doc_file *doc_file,
		struct array *buf)
{
	struct doc_stack_token *tok;
	bool is_reused;
	int d;
	struct doc_iter iter = doc_make_iter(doc_file);
	bool literal = false;

	while((d = doc_iter_walk(&iter, &tok, &is_reused)) != 0) {
		switch(tok->token.type) {
		break; case DocBreak:       do_bit_add(buf, &iter, "\n", d, true);
		break; case DocCode:        do_quote(buf, &iter, d, &literal);
		break; case DocEmbed:       do_path(buf, &iter, tok->token.embed.path, d);
		break; case DocHeading:     do_heading(buf, &iter, tok, d);
		break; case DocList:        do_list(buf, &iter, d);
		break; case DocOrderedList: do_list(buf, &iter, d);
		break; case DocListItem:    do_bit(buf, &iter, "* ", "\n", d, false);
		break; case DocOrderedItem: do_bit(buf, &iter, "* ", "\n", d, false);
		break; case DocPath:        do_path(buf, &iter, tok->token.path.path, d);
		break; case DocQuote:;
		break; case DocText:        do_text(buf, tok, is_reused, d, literal);
		break; case DocParagraph:   do_bit(buf, &iter, "", "\n", d, true);
		break; case DocNone:;
		}
	}

	*((char *)array_add(buf, 1)) = '\0';

	(void)path;
	return true;
}
