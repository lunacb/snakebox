#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "document.h"
#include "mimetype.h"
#include "output/common.h"
#include "util.h"

#include "output/output.h"

#define CLASS(x) \
	"class=\"snx-" x "\""

static void esc_array_add_str(struct array *array, char *str, size_t len)
{
	for(size_t i = 0; i < len; i++) {
		switch(str[i]) {
		break; case '"':  array_add_lit(array, "&quot;");
		break; case '\'': array_add_lit(array, "&apos;");
		break; case '&':  array_add_lit(array, "&amp;");
		break; case '<':  array_add_lit(array, "&lt;");
		break; case '>':  array_add_lit(array, "&gt;");
		break; default: *((char *)array_add(array, 1)) = str[i];
		}
	}
}

static void do_tag(struct array *buf, char *tag, char *extra, int direction)
{
	if(direction == 1) {
		array_add_lit(buf, "<");
		array_add_str(buf, tag, strlen(tag));
		array_add_lit(buf, " ");
		array_add_str(buf, extra, strlen(extra));
		array_add_lit(buf, ">\n");
	} else {
		array_add_lit(buf, "</");
		array_add_str(buf, tag, strlen(tag));
		array_add_lit(buf, ">\n");
	}
}

static void do_tag_add(struct array *buf, char *tag, int direction)
{
	if(direction == 1) {
		array_add_lit(buf, "<");
		array_add_str(buf, tag, strlen(tag));
		array_add_lit(buf, ">\n");
	}
}

static void do_embed(struct array *buf, struct doc_stack_token *tok,
		int direction)
{
	if(direction == 1) {
		char *path = resolve_path(tok->token.embed.path, OutputHtml, NULL,
				NULL, true);

		enum mimetype mimetype = get_mimetype(&tok->token.embed.mime_type);
		switch(mimetype) {
		case MimetypeAudio:
			array_add_lit(buf, "<p "CLASS("embed-p")">");
			array_add_lit(buf, "<audio "CLASS("embed-audio")" controls src=\"");
			esc_array_add_str(buf, path, strlen(path));
			array_add_lit(buf, "\">\n");
			break;

		case MimetypeImage:
			array_add_lit(buf, "<p "CLASS("embed-p")">");
			array_add_lit(buf, "<img "CLASS("embed-image")" src=\"");
			esc_array_add_str(buf, path, strlen(path));
			array_add_lit(buf, "\">\n");
			break;

		case MimetypeVideo:
			array_add_lit(buf, "<p "CLASS("embed-p")">");
			array_add_lit(buf, "<video "CLASS("embed-video")" controls src=\"");
			esc_array_add_str(buf, path, strlen(path));
			array_add_lit(buf, "\">\n");
			break;

		default:
			array_add_lit(buf, "<p "CLASS("embed-p")">");
			array_add_lit(buf, "<a "CLASS("embed-a")" href=\"");
			esc_array_add_str(buf, path, strlen(path));
			array_add_lit(buf, "\">\n");
			break;
		}


		free(path);
	} else {
		enum mimetype mimetype = get_mimetype(&tok->token.embed.mime_type);
		switch(mimetype) {
		break; case MimetypeAudio: array_add_lit(buf, "</audio></p>\n");
		break; case MimetypeImage: array_add_lit(buf, "</img></p>\n");
		break; case MimetypeVideo: array_add_lit(buf, "</video></p>\n");
		break; default: array_add_lit(buf, "</a></p>\n");
		}
	}
}

static void do_heading(struct array *buf, struct doc_stack_token *tok,
		int direction)
{
	char name[] = "hX";
	if(tok->token.heading.level + 1 > 6)
		name[1] = '6';
	else
		name[1] = tok->token.heading.level + '0' + 1;

	if(direction == 1) {
		array_add_lit(buf, "<");
		array_add_lit(buf, name);
		array_add_lit(buf, " "CLASS("heading"));
		array_add_lit(buf, ">\n");
	} else {
		array_add_lit(buf, "</");
		array_add_lit(buf, name);
		array_add_lit(buf, ">\n");
	}
}

static void do_path(struct array *buf, struct doc_stack_token *tok,
		int direction)
{
	if(direction == 1) {
		array_add_lit(buf, "<p "CLASS("path-p")">");
		array_add_lit(buf, "<a "CLASS("path-a")" href=\"");
		char *path = resolve_path(tok->token.path.path, OutputHtml, NULL, NULL,
			true);
		esc_array_add_str(buf, path, strlen(path));
		array_add_lit(buf, "\">\n");
		free(path);
	} else {
		array_add_lit(buf, "</a></p>\n");
	}
}

static void do_text(struct array *buf, struct doc_stack_token *tok,
		bool is_reused, int direction)
{
	if(direction == 1) {
		if(!is_reused) {
			if(tok->token.text.flags & DocTextBold)
				array_add_lit(buf, "\n<b "CLASS("text-bold")">");
			if(tok->token.text.flags & DocTextItalic)
				array_add_lit(buf, "\n<i "CLASS("text-italic")">");
			if(tok->token.text.flags & DocTextCode)
				array_add_lit(buf, "\n<code "CLASS("text-code")">");
		}
		struct slice *text = &tok->token.text.text;
		esc_array_add_str(buf, text->start, text->len);
	} else {
		if(tok->token.text.flags & DocTextCode)
			array_add_lit(buf, "</code>\n");
		if(tok->token.text.flags & DocTextItalic)
			array_add_lit(buf, "</i>\n");
		if(tok->token.text.flags & DocTextBold)
			array_add_lit(buf, "</b>\n");
	}
}

bool output_html_process(char *path, struct doc_file *doc_file,
		struct array *buf)
{
	struct doc_stack_token *tok;
	bool is_reused;
	int d;
	struct doc_iter iter = doc_make_iter(doc_file);

	while((d = doc_iter_walk(&iter, &tok, &is_reused)) != 0) {
		switch(tok->token.type) {
		break; case DocBreak:       do_tag_add(buf, "br "CLASS("break"), d);
		break; case DocCode:        do_tag(buf, "pre", CLASS("code"), d);
		break; case DocEmbed:       do_embed(buf, tok, d);
		break; case DocHeading:     do_heading(buf, tok, d);
		break; case DocList:        do_tag(buf, "ul", CLASS("list"), d);
		break; case DocOrderedList: do_tag(buf, "ol", CLASS("ordered-list"), d);
		break; case DocListItem:    do_tag(buf, "li", CLASS("list-item"), d);
		break; case DocOrderedItem: do_tag(buf, "li", CLASS("ordered-item"), d);
		break; case DocPath:        do_path(buf, tok, d);
		break; case DocQuote:       do_tag(buf, "blockquote", CLASS("quote"), d);
		break; case DocText:        do_text(buf, tok, is_reused, d);
		break; case DocParagraph:   do_tag(buf, "p", CLASS("paragraph"), d);
		break; case DocNone:;
		}
	}

	*((char *)array_add(buf, 1)) = '\0';

	(void)path;
	return true;
}
