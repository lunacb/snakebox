#include <stdio.h>
#include <string.h>
#include "document.h"
#include "output/output.h"
#include "util.h"

bool output_rss_process(char *path, struct doc_file *doc_file,
		struct array *buf)
{
	if(output_html_process(path, doc_file, buf) == false)\
		return false;

	size_t old_len = buf->len;
	size_t final_len = buf->len;
	char *str = buf->data;
	for(size_t i = 0; i < buf->len; i++) {
		switch(str[i]) {
		break; case '"':  final_len += sizeof("&quot;") - 2;
		break; case '\'': final_len += sizeof("&apos;") - 2;
		break; case '&':  final_len += sizeof("&amp;") - 2;
		break; case '<':  final_len += sizeof("&lt;") - 2;
		break; case '>':  final_len += sizeof("&gt;") - 2;
		}
	}

	if(old_len == 0)
		return true;

	array_add(buf, final_len - old_len);
	str = buf->data;
	size_t i, j;
	for(i = old_len - 1, j = final_len - 1; ; i--, j--) {
		switch(str[i]) {
		break; case '"':  j -= sizeof("&quot;") - 2;
			strcpy(&str[j], "&quot;");
		break; case '\'': j -= sizeof("&apos;") - 2;
			strcpy(&str[j], "&apos;");
		break; case '&':  j -= sizeof("&amp;") - 2;
			strcpy(&str[j], "&amp;");
		break; case '<':  j -= sizeof("&lt;") - 2;
			strcpy(&str[j], "&lt;");
		break; case '>':  j -= sizeof("&gt;") - 2;
			strcpy(&str[j], "&gt;");
		break; default:
			str[j] = str[i];
		}

		if(i == j)
			break;
	}

	return true;
}
