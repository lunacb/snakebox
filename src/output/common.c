#include "fs.h"
#include "interface.h"
#include "slice.h"
#include "sliceop.h"

#include "output/common.h"

char *resolve_path(struct slice path, enum outputs output, bool *is_url,
		bool *is_doc, bool do_prefix)
{
	if(is_url)
		*is_url = false;
	if(is_doc)
		*is_doc = true;

	if(path.start[0] != '(' || path.start[path.len - 1] != ')' ||
			path.len <= 2) {
		if(is_url)
			*is_url = true;
		return compile_str(((struct slice[]){path}));
	}

	struct slice new = path;
	// trim off the parentheses
	new.start += 1;
	new.len -= 2;

	return fs_doc_to_dest(new, output, is_doc, do_prefix);
}
