#include <stdbool.h>
#include <string.h>
#include "document.h"
#include "mimetype.h"
#include "output/common.h"
#include "sliceop.h"
#include "util.h"

#include "output/output.h"

// TODO: allow customizatoin of this
#define TABWIDTH 8
#define LINEWIDTH 67

enum line_mode {
	LineNormal,
	LineSingle,
	LineCode,
};
struct gopher_buf {
	// how many characters are on the current line
	int line_width;
	int space_pad;
	struct array *buf;
	size_t linepos;
	enum line_mode mode;
	// stored path info
	struct {
		bool prefirst;
		bool slashfirst;
		struct slice first;
		struct slice second;
		char *str;
	} stored;
};

static int update_line_width(struct gopher_buf *buf)
{
	char *data = buf->buf->data;
	size_t i;
	for(i = buf->buf->len; i != buf->linepos; i--) {
		if(data[i-1] == '\n') {
			buf->line_width = 0;
			break;
		}
	}
	buf->line_width += buf->buf->len - i;
	buf->linepos = buf->buf->len;

	return buf->line_width;
}

static void start_line_quote(struct gopher_buf *buf, int level)
{
	int total_pad = buf->space_pad;

	if(level) {
		memset(array_add(buf->buf, level), '>', level);
		total_pad++;
	}

	memset(array_add(buf->buf, total_pad), ' ', total_pad);
}

static inline void start_line(struct gopher_buf *buf, struct doc_iter *iter)
{
	start_line_quote(buf, iter->quote_level);
}

static inline bool should_wrap_line(struct gopher_buf *buf, size_t to_add)
{
	if(to_add + update_line_width(buf) > LINEWIDTH)
		return true;
	else
		return false;
}

static bool wrap_line(struct gopher_buf *buf, struct doc_iter *iter,
		size_t to_add)
{
	if(!should_wrap_line(buf, to_add))
			return false;

	array_add_lit(buf->buf, "\n");

	start_line(buf, iter);
	return true;
}

static inline void do_tab(struct gopher_buf *buf)
{
	int len = TABWIDTH - (update_line_width(buf) % TABWIDTH);
	memset(array_add(buf->buf, len), ' ', len);
}

static void do_bit(struct gopher_buf *buf, struct doc_iter *iter, char *add,
		char *rm, int direction)
{
	int addlen = strlen(add);

	if(direction == 1) {
		start_line(buf, iter);
		buf->space_pad += addlen;
		array_add_str(buf->buf, add, addlen);
	} else {
		buf->space_pad -= addlen;
		array_add_str(buf->buf, rm, strlen(rm));
	}
}

static void do_path(struct gopher_buf *buf, struct doc_iter *iter,
		struct slice tok_path, enum mimetype mimetype, int direction)
{
	if(direction == 1) {
		bool is_url;
		bool is_doc;
		char *path = resolve_path(tok_path, OutputGopher, &is_url,
			&is_doc, false);
		buf->mode = LineSingle;
		buf->stored.str = path;

		start_line(buf, iter);

		if(is_url) {
			if(!strncmp(path, "gopher://", sizeof("gopher://") - 1)) {
				char *start = path + sizeof("gopher://") - 1;
				char *chr;
				buf->stored.prefirst = false;

				if(!(chr = strchr(start, '/'))) {
					buf->stored.slashfirst = true;
					buf->stored.first = (struct slice){ 0, 0 };
					buf->stored.second = (struct slice)str_slice(start);
				} else {
					buf->stored.slashfirst = false;
					buf->stored.first = (struct slice)str_slice(chr);
					buf->stored.second = (struct slice){
						.start = start,
						.len = chr - start,
					};
				}

				switch(mimetype) {
				break; case MimetypeAudio: array_add_lit(buf->buf, "<");
				break; case MimetypeImage: array_add_lit(buf->buf, "I");
				break; case MimetypeVideo: array_add_lit(buf->buf, ";");
				break; default: array_add_lit(buf->buf, "1");
				}
			} else {
				buf->stored.slashfirst = false;
				buf->stored.prefirst = true;
				buf->stored.first = (struct slice)str_slice(path);
				buf->stored.second = (struct slice){ 0, 0 };

				array_add_lit(buf->buf, "h");
			}
		} else {
			buf->stored.slashfirst = false;
			buf->stored.prefirst = false;
			buf->stored.first = (struct slice)str_slice(path);
			buf->stored.second = (struct slice){ 0, 0 };

			if(is_doc) {
				array_add_lit(buf->buf, "1");
			} else {
				switch(mimetype) {
				break; case MimetypeAudio: array_add_lit(buf->buf, "<");
				break; case MimetypeImage: array_add_lit(buf->buf, "I");
				break; case MimetypeVideo: array_add_lit(buf->buf, ";");
				break; default: array_add_lit(buf->buf, "9");
				}
			}
		}
	} else {
		buf->mode = LineNormal;
		array_add_lit(buf->buf, "\t");

		if(buf->stored.prefirst)
			array_add_lit(buf->buf, "URL:");

		// here, we just have a domain name so we're going to the root (/)
		// document
		if(buf->stored.slashfirst)
			array_add_lit(buf->buf, "/");

		if(buf->stored.first.start)
			array_add_str(buf->buf, buf->stored.first.start,
				buf->stored.first.len);

		if(buf->stored.second.start) {
			array_add_lit(buf->buf, "\t");
			array_add_str(buf->buf, buf->stored.second.start,
				buf->stored.second.len);
		}
		array_add_lit(buf->buf, "\n");

		free(buf->stored.str);
	}
}

static void do_embed(struct gopher_buf *buf, struct doc_iter *iter,
		struct doc_stack_token *tok, int direction)
{
	do_path(buf, iter, tok->token.embed.path,
		get_mimetype(&tok->token.embed.mime_type), direction);
}

static void do_heading(struct gopher_buf *buf, struct doc_iter *iter,
		struct doc_stack_token *tok, int direction)
{
	char hbuf[tok->token.heading.level + 2];
	hbuf[sizeof(hbuf) - 2] = ' ';
	hbuf[sizeof(hbuf) - 1] = '\0';
	memset(hbuf, '#', sizeof(hbuf) - 2);
	do_bit(buf, iter, hbuf, "\n", direction);
}

static void do_text(struct gopher_buf *buf, struct doc_iter *iter,
		struct doc_stack_token *tok, bool is_reused, int direction)
{
	struct slice *text = &tok->token.text.text;
	struct slice stok = slicetok_init(text);

	if(direction == 1) {
		if(!is_reused) {
			char fancy[4];
			int flen = 0;
			if(tok->token.text.flags & DocTextBold)
				fancy[flen++] = '*';
			if(tok->token.text.flags & DocTextItalic)
				fancy[flen++] = '*';
			if(tok->token.text.flags & DocTextCode)
				fancy[flen++] = '`';

			if(buf->mode == LineNormal)
				wrap_line(buf, iter, flen);

			// TODO: make this always put the fancy on the same line as part of
			// the text
			array_add_str(buf->buf, fancy, flen);
		}

		if(buf->mode == LineCode) {
			for(size_t i = 0; i < text->len; i++) {
				if(text->start[i] == '\t') {
					do_tab(buf);
				} else {
					array_add_str(buf->buf, &text->start[i], 1);
					if(text->start[i] == '\n' && i + 1 < text->len)
						start_line(buf, iter);
				}
			}
		} else {
			while(slicetok(text, &stok)) {
				if(buf->mode == LineNormal) {
					wrap_line(buf, iter, stok.len);
					array_add_str(buf->buf, stok.start, stok.len);
				} else {
					for(size_t i = 0; i < stok.len; i++) {
						if(stok.start[i] == '\n')
							continue;
						array_add_str(buf->buf, &stok.start[i], 1);
					}
				}
				if(buf->mode == LineSingle || !should_wrap_line(buf, 1))
					array_add_lit(buf->buf, " ");
			}
		}
	} else {
		if(buf->buf->len && ((char *)buf->buf->data)[buf->buf->len - 1] == ' ')
			buf->buf->len--;

		char fancy[4];
		int flen = 0;
		if(tok->token.text.flags & DocTextCode)
			fancy[flen++] = '`';
		if(tok->token.text.flags & DocTextItalic)
			fancy[flen++] = '*';
		if(tok->token.text.flags & DocTextBold)
			fancy[flen++] = '*';


		if(buf->mode == LineNormal)
			wrap_line(buf, iter, flen);

		array_add_str(buf->buf, fancy, flen);

		if(buf->mode == LineSingle || !should_wrap_line(buf, 1))
			array_add_lit(buf->buf, " ");
	}
}

static void do_break(struct gopher_buf *buf, struct doc_iter *iter,
		int direction)
{
	if(direction == 1) {
		start_line(buf, iter);
		array_add_lit(buf->buf, "\n");
	}
}

static void do_code(struct gopher_buf *buf, struct doc_iter *iter,
		int direction)
{
	buf->mode = (direction == 1)?LineCode:LineNormal;
	do_bit(buf, iter, "", "\n", direction);
}

bool output_gopher_process(char *path, struct doc_file *doc_file,
		struct array *buf)
{
	struct array startbuf = array_init(sizeof(char));

	struct gopher_buf b = {
		.line_width = 0,
		.space_pad = 0,
		.buf = &startbuf,
		.linepos = 0,
		.mode = LineNormal,
	};
	struct doc_stack_token *tok;
	bool is_reused;
	int d;
	struct doc_iter iter = doc_make_iter(doc_file);

	enum doc_token_type last_tok = DocNone;
	int last_d = 0;
	bool vspace_added = false;
	int last_quote = iter.quote_level;

	size_t used_buf_len = startbuf.len;

	while((d = doc_iter_walk(&iter, &tok, &is_reused)) != 0) {

		if((last_tok == DocBreak ||
				last_tok == DocCode ||
				last_tok == DocEmbed ||
				last_tok == DocHeading ||
				last_tok == DocList ||
				last_tok == DocOrderedList ||
				last_tok == DocPath ||
				last_tok == DocQuote ||
				last_tok == DocParagraph) &&
				last_d == -1) {
			if(!vspace_added) {
				start_line_quote(&b, last_quote);
				array_add_lit(&startbuf, "\n");
				vspace_added = true;
			}
		} else {
			vspace_added = false;
		}

		size_t old_buf_len = startbuf.len;

		switch(tok->token.type) {
		break; case DocBreak:        do_break(&b, &iter, d);
		break; case DocCode:         do_code(&b, &iter, d);
		break; case DocEmbed:        do_embed(&b, &iter, tok, d);
		break; case DocHeading:      do_heading(&b, &iter, tok, d);
		break; case DocList:;
		break; case DocOrderedList:;
		break; case DocListItem:     do_bit(&b, &iter, "* ", "\n", d);
		break; case DocOrderedItem:  do_bit(&b, &iter, "* ", "\n", d);
		break; case DocPath:        do_path(&b, &iter, tok->token.path.path, MimetypeNone, d);
		break; case DocQuote:;
		break; case DocText:         do_text(&b, &iter, tok, is_reused, d);
		break; case DocParagraph:    do_bit(&b, &iter, "", "\n", d);
		break; case DocNone:;
		}

		last_tok = tok->token.type;
		last_d = d;
		last_quote = iter.quote_level;

		if(old_buf_len != startbuf.len)
			used_buf_len = startbuf.len;
	}

	startbuf.len = used_buf_len;

	// Postprocessing step to formalize info text lines a bit. This would be
	// done as we were compiling the gopehrfile if the code was less messy and
	// bad.
	char *data = startbuf.data;
	for(size_t i = 0; i < startbuf.len; i++) {
		bool tab_this_line = false;
		size_t j;
		for(j = i; j < startbuf.len && data[j] != '\n'; j++)
			if(data[j] == '\t')
				tab_this_line = true;
		if(tab_this_line) {
			array_add_str(buf, &data[i], j - i);
		} else {
			array_add_lit(buf, "i");
			array_add_str(buf, &data[i], j - i);
			array_add_lit(buf, "\t/");
		}
		if(data[j] == '\n')
			array_add_lit(buf, "\n");

		i = j;
	}
	free(startbuf.data);

	*((char *)array_add(buf, 1)) = '\0';

	(void)path;
	return true;
}
