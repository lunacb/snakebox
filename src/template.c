#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "def.h"
#include "log.h"
#include "util.h"

#include "template.h"

#define READ_SIZE 2048

// Test for substring literal `lit` in string `str` at position `i`. Equivalent
// return value to strcmp.
#define SUBSTR(buf, lit, i) \
	strncmp(&buf[i], lit, sizeof(lit) - 1)

// State during parsing.
struct state {
	size_t i;
	size_t text_start;
};

static inline struct template_segment text_seg(char *start, size_t len)
{
	return (struct template_segment){
		.type = TemplateText,
		.text = {
			.start = start,
			.len = len,
		},
	};
}

void scope(struct template_ctx *ctx, struct state *state, bool is_root,
		struct template_segment *parent_seg);

bool template_init(struct template_ctx *ctx, FILE *in)
{
	ctx->segments = array_init(sizeof(struct template_segment));

	struct array buf = array_init(sizeof(char));

	// Dump contents of `in` into a buffer.
	errno = 0;
	int c;
	while((c = fgetc(in)) != EOF)
		*((char *)array_add(&buf, 1)) = c;
	*((char *)array_add(&buf, 1)) = '\0';
	if(errno) {
		LOG_ERRNO("Failed to read from template file");
		return false;
	}

	ctx->in = buf;
	char *instr = buf.data;

	struct state state = { .i = 0, .text_start = 0 };
	scope(ctx, &state, true, NULL);

	// Add any trailing text.
	if(buf.len - 1 > state.text_start) {
		*((struct template_segment *)array_add(&ctx->segments, 1)) =
			text_seg(&instr[state.text_start], buf.len - 1 - state.text_start);
	}

	return true;
}

void scope(struct template_ctx *ctx, struct state *state, bool is_root,
		struct template_segment *parent_seg)
{
	if(!is_root)
		parent_seg->array.have_delim = false;

	size_t inlen = ctx->in.len - 1;
	size_t text_start = state->text_start;
	char *in = ctx->in.data;

	for(size_t i = state->i; i < inlen; i++) {
		if(!SUBSTR(in, "@@{", i)) {
			// Position where the string was matched.
			size_t before = i;
			// Start of the variable name.
			size_t start = i + sizeof("@@{") - 1;
			for(i = start; i < inlen; i++) {
				if(VAR_CHAR_IS_VALID(in[i])) { continue; }
				// If this doesn't look like a variable, treat as normal text
				// and leave.
				if(SUBSTR(in, "}@@", i) != 0 || i == start) { break; }

				// Add any text between this and the last directive.
				if(before > text_start) {
					*((struct template_segment *)array_add(&ctx->segments, 1)) =
						text_seg(&in[text_start], before - text_start);
				}
				text_start = i + sizeof("}@@") - 1;

				*((struct template_segment *)array_add(&ctx->segments, 1)) =
					(struct template_segment){
						.type = TemplateVar,
						.var = {
							.name = &in[start],
						},
					};
				// NULL terminator for the variable name.
				in[i] = '\0';

				i += sizeof("}@@") - 1 - 1;

				break;
			}
		} else if(!SUBSTR(in, "@@[", i)) {
			// Position where the string was matched.
			size_t before = i;
			// Start of te variable name.
			size_t start = i + sizeof("@@[") - 1;
			for(i = start; i < inlen; i++) {
				if(VAR_CHAR_IS_VALID(in[i])) { continue; }
				// If this doesn't look like a normal array, treat as normal
				// text and leave.
				if(in[i] != '|' || i == start) { break; }

				size_t name_end = i;

				// Add any text between this and the last directive.
				if(before > text_start) {
					*((struct template_segment *)array_add(&ctx->segments, 1)) =
						text_seg(&in[text_start], before - text_start);
				}
				text_start = i + 1;

				// Will either be a TemplateArray item, or a TemplateText item
				// containing the previously matched text if there isn't a
				// closing statement.
				struct template_segment *array_seg = array_add(&ctx->segments, 1);

				// The `scope` function will fill out the `array.have_delim`
				// and `array.delim_start` fields of `array_seg`.
				array_seg->type = TemplateArray;

				struct state end_state = {
					.i = i,
					.text_start = text_start,
				};
				scope(ctx, &end_state, false, array_seg);
				// If there was a delimiter found but it was empty, remove it.
				if(array_seg->array.have_delim &&
						array_seg->array.delim_start > ctx->segments.len - 1) {
					array_seg->array.have_delim = false;
				}
				if(end_state.i < inlen) {
					// Closing statement found. This is a valid array.

					*array_seg = (struct template_segment){
							.type = TemplateArray,
							.array = {
								.name = &in[start],
								.last = ctx->segments.len - 1,
								.have_delim = array_seg->array.have_delim,
								.delim_start = array_seg->array.delim_start,
							}
						};
					// NULL terminator for the array name.
					in[name_end] = '\0';

					i = end_state.i + sizeof("]@@") - 1 - 1;
					text_start = end_state.i + sizeof("]@@") - 1;
				} else {
					// A matching closing statement wasn't found and we're at
					// the end of the file, so add the beginning of the array
					// as text.
					*array_seg = text_seg(&in[before], i - (name_end + 1));

					i = end_state.i;
					text_start = end_state.text_start;
				}
				break;
			}
		} else if(!is_root && !parent_seg->array.have_delim && !SUBSTR(in, "]@@[", i)) {
			// Add any text between this and the last directive.
			if(i > text_start) {
				*((struct template_segment *)array_add(&ctx->segments, 1)) =
					text_seg(&in[text_start], i - text_start);
			}
			text_start = i + sizeof("]@@[") - 1;

			i += sizeof("]@@[") - 1 - 1;

			parent_seg->array.have_delim = true;
			parent_seg->array.delim_start = ctx->segments.len;
		} else if(!is_root && !SUBSTR(in, "]@@", i)) {
			// Add any text between this and the last directive.
			if(i > text_start) {
				*((struct template_segment *)array_add(&ctx->segments, 1)) =
					text_seg(&in[text_start], i - text_start);
			}
			text_start = i + sizeof("}@@") - 1;

			state->i = i;
			state->text_start = text_start;
			return;
		}
	}

	state->i = inlen;
	state->text_start = text_start;
	return;
}

void template_deinit(struct template_ctx *ctx)
{
	free(ctx->in.data);
	free(ctx->segments.data);
}

struct template_walker template_walker_init_array(struct template_ctx *ctx,
		struct template_segment *array)
{
	struct template_segment *segs = ctx->segments.data;
	struct template_walker res = {
		.ctx = ctx,
		.first = array + 1,
		.end = &segs[array->array.last + 1],
	};

	if(array->array.have_delim) {
		res.pos = array + 1;
		res.delim_start = &segs[array->array.delim_start];
	} else {
		res.pos = NULL;
		res.delim_start = array + 1;
	}

	return res;
}

struct template_walker template_walker_init(struct template_ctx *ctx)
{
	struct template_segment *segs = ctx->segments.data;
	return (struct template_walker){
		.ctx = ctx,
		.pos = NULL,
		.first = segs,
		.end = &segs[ctx->segments.len],
		.delim_start = segs,
	};
}

struct template_segment *template_walk(struct template_walker *walker)
{
	struct template_segment *segs = walker->ctx->segments.data;

	if(walker->pos == walker->delim_start) {
		walker->pos = NULL;
		return NULL;
	}

	if(walker->pos == NULL) {
		walker->pos = walker->delim_start;
	}

	struct template_segment *res = walker->pos;

	if(walker->pos->type == TemplateArray)
		walker->pos = &segs[walker->pos->array.last + 1];
	else
		walker->pos++;

	if(walker->pos >= walker->end)
		walker->pos = walker->first;

	return res;
}
