#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

struct array array_init(size_t size)
{
	return (struct array){
		.data = NULL,
		.len = 0,
		.cap = 0,
		.size = size,
	};
}

void array_atleast_cap(struct array *array, size_t cap)
{
	if(array->cap > cap)
		return;

	if(array->cap == 0) {
		array->cap = cap;
	} else  {
		while(array->cap < cap)
			array->cap *= 2;
	}

	array->data = xrealloc(array->data, array->cap * array->size);
}

void *array_add(struct array *array, size_t n)
{
	array_atleast_cap(array, array->len + n);
	void *ptr = array->data + array->len * array->size;
	array->len += n;

	return ptr;
}

void array_rm_messy(struct array *array, size_t i)
{
	size_t size = array->size;

	memmove(array->data + size * i,
		array->data + (array->len - size) * size, size);
	array->len -= 1;
}


void *alloc_test(const char *file, int line, void *ptr)
{
	errno = 0;
	if(ptr == NULL && errno) {
		fprintf(stderr, "%s:%d: Failed to allocate memory: %s\n",
			file, line, strerror(errno));
		abort();
	}

	return ptr;
}
