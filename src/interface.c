#include <stdbool.h>

#include "output/output.h"
#include "target/target.h"

#include "interface.h"

#ifdef TEST
#define FN(x) NULL
#else
#define FN(x) x
#endif

const struct names names = {
	.target = {
		[TargetDocument] = "document",
		[TargetIndex] = "index",
		[TargetTagIndex] = "tag_index",
	},
	.output = {
		[OutputHtml] = "html",
		[OutputGemini] = "gemini",
		[OutputGopher] = "gopher",
		[OutputRss] = "rss",
	},
	.template = {
		[TemplateDoc] = "doc",
		[TemplateIndex] = "index",
		[TemplateTagIndex] = "tag_index",
		[TemplateTagSingleIndex] = "tag_single_index",
	},
};

const struct output_info output_info[OutputLast] = {
	[OutputHtml] = {
		.suffix = ".html",
		.file_suffix = ".html",
		.func_process = FN(output_html_process),
	},
	[OutputGemini] = {
		.suffix = ".gmi",
		.file_suffix = ".gmi",
		.func_process = FN(output_gemini_process),
	},
	[OutputGopher] = {
		.suffix = "",
		.file_suffix = "/gophermap",
		.func_process = FN(output_gopher_process),
	},
	[OutputRss] = {
		.suffix = ".xml",
		.file_suffix = ".xml",
		.func_process = FN(output_rss_process),
	},
};

const struct target_info target_info[TargetLast] = {
		[TargetDocument] = {
			.templates = (enum templates[]){
				TemplateDoc,
				TemplateLast,
			},
			.func_init = FN(target_document_init),
			.func_deinit = FN(target_document_deinit),
			.func_deinit_err = FN(target_document_deinit_err),
			.func_process = FN(target_document_process),
		},
		[TargetIndex] = {
			.templates = (enum templates[]){
				TemplateIndex,
				TemplateLast,
			},
			.func_init = FN(target_index_init),
			.func_deinit = FN(target_index_deinit),
			.func_deinit_err = FN(target_index_deinit_err),
			.func_process = FN(target_index_process),
		},
		[TargetTagIndex] = {
			.templates = (enum templates[]){
				TemplateTagIndex,
				TemplateTagSingleIndex,
				TemplateLast,
			},
			.func_init = FN(target_tag_index_init),
			.func_deinit = FN(target_tag_index_deinit),
			.func_deinit_err = FN(target_tag_index_deinit_err),
			.func_process = FN(target_tag_index_process),
		},
};

struct path_opt *path_opts = (struct path_opt[]){
	{
		.file = PathFileIndex,
		.opt_name = "index-file",
		.help =
"  --index-file PATH  index file relative to destination without a suffix\n",
		.value = "index",
	},
	{
		.file = PathFileTagIndex,
		.opt_name = "tag-index-file",
		.help =
"  --tag-index-file PATH  tag index file relative to destination without a suffix\n",
		.value = "tags",
	},
	{
		.file = PathFileTagDir,
		.opt_name = "tag-dir",
		.help =
"  --tag-dir PATH  directory for tag files relative to destination\n",
		.value = ".",
	},
	{ .file = PathFileLast },
};

struct path_varname path_varnames[PathFileLast] = {
	[PathFileIndex] =
		{ .absolute = "index-path",      .relative = "index-relative-path" },
	[PathFileTagIndex] =
		{ .absolute = "tag-index-path",  .relative = "tag-index-relative-path" },
	[PathFileTagDir] =
		{ .absolute = NULL,              .relative = NULL },
	[PathFileSingleTag] =
		{ .absolute = "tag-path",        .relative = "tag-relative-path" },
	[PathFileDoc] =
		{ .absolute = "doc-path",        .relative = "doc-relative-path" },
};
