#define _POSIX_C_SOURCE 200809L

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fs.h"
#include "interface.h"
#include "log.h"
#include "run.h"
#include "template.h"
#include "version.h"

#include "main.h"

struct template_ctx template_map[OutputLast][TemplateLast];
bool used_targets[TargetLast];
bool used_outputs[OutputLast];
char *program_name;

char *date_read_fmt;
char *date_write_fmt;

bool input_ok = true;

static void deinit(void)
{
	fs_deinit();
	for(enum outputs i = 0; i < OutputLast; i++)
		for(enum templates j = 0; j < TemplateLast; j++)
			template_deinit(&template_map[i][j]);
}

static void print_help(void)
{
	printf("\n"
		"Usage: %s --targets list [options]\n"
		"Usage: %s --help\n"
		"Usage: %s --version\n"
		"  -h  --help                       print this text\n"
		"  -v  --version                    print the version\n"
		"  -t  --targets LIST               comma-separated list of targets\n"
		"  -x  --template DIR               add a template directory\n"
		"  -x  --template OUTPUT:DIR        add a template directory for an output\n"
		"  -x  --template OUTPUT:NAME=PATH  add a template for an output\n"
		"  -o  --outputs LIST               comma-separated list of outputs\n"
		"  -s  --source DIR                 source directory\n"
		"  -d  --dest DIR                   destination directory\n"
		"  -d  --dest OUTPUT:DIR            destination directory for an output\n"
		"  -p  --prefix OUTPUT:PREFIX       prefix for absolute paths in an output\n"
		"  -V  --var                        TODO\n"
		"  --date-read-fmt FORMAT           format to read dates from markup files\n"
		"  --date-write-fmt FORMAT          format to write dates to markup files\n"
		"\n"
		, program_name, program_name, program_name);

	for(enum path_files i = 0; path_opts[i].file != PathFileLast; i++)
		fputs(path_opts[i].help, stdout);

	printf("\nOutputs: ");
	for(enum outputs i = 0; ; i++) {
		if(i == OutputLast - 1) {
			printf("\"%s\"\n", names.output[i]);
			break;
		} else {
			printf("\"%s\", ", names.output[i]);
		}
	}

	printf("\nTargets:\n");
	for(enum targets i = 0; i < TargetLast; i++) {
		printf("  \"%s\" (Templates: ", names.target[i]);
		for(enum templates *j = target_info[i].templates; ; j++) {
			if(j[1] == TemplateLast) {
				printf("\"%s\")\n", names.template[*j]);
				break;
			} else {
				printf("\"%s\", ", names.template[*j]);
			}
		}
	}

	printf("\nAlso see snx(1).\n");
}

static void help(void)
{
	print_help();
	deinit();
	exit(1);
}

static void version(void)
{
	printf("snx " VERSION "\n");
	deinit();
	exit(0);
}

static bool set_targets(bool used_templates[TemplateLast], char *str)
{
	enum targets target;

	for(char *tok = strtok(str, ","); tok; tok = strtok(NULL, ",")) {
		if(!try_enumerate(target, tok, names.target, TargetLast)) {
			LOG_ERR("Target %s does not exist", tok);
			return false;
		}
		used_targets[target] = true;

		enum templates *tmps = target_info[target].templates;
		for(enum templates *ptr = tmps; *ptr != TemplateLast; ptr++) {
			used_templates[*ptr] = true;
		}
	}

	return true;
}

static bool set_outputs(char *str)
{
	enum outputs output;

	memset(used_outputs, false, sizeof(bool) * OutputLast);

	for(char *tok = strtok(str, ","); tok; tok = strtok(NULL, ",")) {
		if(!try_enumerate(output, tok, names.output, OutputLast)) {
			LOG_ERR("Output %s does not exist", tok);
			return false;
		}
		used_outputs[output] = true;
	}

	return true;
}

static bool add_template(char *str)
{
	char *c1, *c2;

	if((c1 = strchr(str, ':')) != NULL) {
		*c1 = '\0';
		if((c2 = strchr(c1 + 1, '=')) != NULL) {
			*c2 = '\0';
			if(!fs_add_output_template(str, c1+1, c2+1))
				return false;
		} else {
			if(!fs_add_output_template_dir(str, c1+1))
				return false;
		}
	} else {
		if(!fs_add_template_dir(str))
			return false;
	}

	return true;
}

static bool parse_templates(bool used_templates[TemplateLast])
{
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(!used_outputs[i])
			continue;

		for(enum templates j = 0; j < TemplateLast; j++) {
			if(!used_templates[j])
				continue;

			if(gpaths.output[i].templates[j] != NULL) {
				FILE *file = fopen(gpaths.output[i].templates[j], "r");
				if(file == NULL) {
					LOG_ERRNO("Failed to open file %s",
							gpaths.output[i].templates[j]);
					return false;
				}
				if(!template_init(&template_map[i][j], file))
					return false;
				fclose(file);
			} else {
				input_ok = false;
				LOG_ERR("Required template %s for output %s not specified",
						names.template[j], names.output[i]);
				return false;
			}
		}
	}

	return true;
}

static bool set_dest(char *str)
{
	char *ptr;
	if((ptr = strchr(str, ':')) != NULL) {
		*ptr = '\0';
		return fs_set_output_dest(str, ptr+1);
	} else {
		return fs_set_dest(str);
	}

	return false;
}

static bool set_prefix(char *str)
{
	char *ptr;
	if((ptr = strchr(str, ':')) == NULL) {
		LOG_ERR("Invalid prefix format for %s", str);
		return false;
	}
	*ptr = '\0';

	return fs_set_prefix(str, ptr+1);
}

static inline bool is_used(bool *list, size_t len)
{
	for(size_t i = 0; i < len; i++)
		if(list[i])
			return true;
	return false;
}

int main(int argc, char **argv)
{
	fs_init();

	date_read_fmt = "%Y-%m-%d %H:%M:%S %Z";
	date_write_fmt = "%Y-%m-%d %H:%M:%S %Z";

	program_name = argv[0];

	static struct option const_opts[] = {
		{ "help", no_argument, 0, 'h' },
		{ "version", no_argument, 0, 'v' },
		{ "template", required_argument, 0, 'x' },
		{ "targets", required_argument, 0, 't' },
		{ "outputs", required_argument, 0, 'o' },
		{ "source", required_argument, 0, 's' },
		{ "dest", required_argument, 0, 'd' },
		{ "prefix", required_argument, 0, 'p' },
		{ "date-read-fmt", required_argument, 0, 1},
		{ "date-write-fmt", required_argument, 0, 2},
		{ 0, 0, 0, 0 },
	};

	static const size_t const_optsize = sizeof(const_opts)/sizeof(*const_opts);
	const int first_path_opt = const_opts[const_optsize - 2].val + 1;
	// Get the size of the path_opts array.
	size_t add_optsize;
	for(add_optsize = 0; path_opts[add_optsize].file != PathFileLast;
			add_optsize++)
		;

	struct option long_opts[const_optsize + add_optsize];
	for(enum path_files i = 0; path_opts[i].file != PathFileLast; i++)
		long_opts[i] = (struct option){ path_opts[i].opt_name,
			required_argument, 0, path_opts[i].file + first_path_opt },
	memcpy(&long_opts[add_optsize], &const_opts, sizeof(const_opts));

	bool used_templates[TemplateLast];

	memset(used_targets, false, sizeof(used_targets));
	memset(used_outputs, false, sizeof(used_outputs));
	memset(used_templates, false, sizeof(used_templates));

	int c;

	while((c = getopt_long(argc, argv, "hvx:t:o:s:d:p:",
					long_opts, NULL)) != -1) {
		switch(c) {
		break; case 'x': if(!add_template(optarg))goto error;
		break; case 't': if(!set_targets(used_templates, optarg))goto error;
		break; case 'o': if(!set_outputs(optarg))goto error;
		break; case 's': gpaths.src_from_cwd = optarg;
		break; case 'd': if(!set_dest(optarg))goto error;
		break; case 'p': if(!set_prefix(optarg))goto error;
		break; case 'h': help();
		break; case 'v': version();
		break; case 1:   date_read_fmt = optarg;
		break; case 2:   date_write_fmt = optarg;
		break; default:;
			int pc = c - first_path_opt;
			if(pc >= 0 && pc < PathFileLast) {
				path_files[pc] = optarg;
				break;
			} else {
				help();
			}
		}
	}

	// TODO: validate date_read_fmt and date_write_fmt

	if(!parse_templates(used_templates))goto error;

	if(!is_used(used_targets, TargetLast)) {
		LOG_ERR("No targets are set");
		input_ok = false;
		goto error;
	}
	if(!is_used(used_outputs, OutputLast)) {
		LOG_ERR("No outputs are set");
		input_ok = false;
		goto error;
	}
	for(enum outputs i = 0; i < OutputLast; i++) {
		if(used_outputs[i] && gpaths.output[i].dest_from_cwd == NULL) {
			LOG_ERR("No destination specified for output %s", names.output[i]);
			input_ok = false;
			goto error;
		}
	}
	if(gpaths.src_from_cwd == NULL) {
		LOG_ERR("No source file specified");
		input_ok = false;
		goto error;
	}

	run();

	deinit();
	return 0;

error:
	if(!input_ok)
		print_help();

	deinit();

	return 1;
}
