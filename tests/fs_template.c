#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/stat.h>

#include "src/fs.c"

#define S(x) \
	"/tmp/.test/" x

bool input_ok = true;
char *program_name;

char *dirs[] = {
	S(""),
	S("output"),
	S("output/html"),
	S("output/html/index"),
	S("output/html2"),
	S("output/fake"),
	S("output/empty"),
	S("big"),
	S("big/gopher"),
	S("big/html"),
};

char *files[] = {
	S("output/html/doc"),
	S("output/html/preview"),
	S("output/html2/preview"),
	S("output/html2/index"),
	S("output/fake/preview"),
	S("output/big/gopher"),
	S("big/gopher/preview"),
	S("big/gopher/index"),
	S("big/gopher/useless"),
	S("big/html/doc"),
};

#include "fs.h"

static bool outstr_exists(char *cmpstr)
{
	for(int i = 0; i < OutputLast; i++) {
		for(int j = 0; j < TemplateLast; j++) {
			char *str = gpaths.output[i].templates[j];
			if(str != NULL && !strcmp(str, cmpstr))
				return true;
		}
	}

	return false;
}

int main(int argc, char **argv)
{
	(void)argc;
	program_name = argv[0];

	fs_init();
	make_files();

	assert(fs_add_output_template_dir("html", S("output/html")));
	assert(!strcmp(gpaths.output[OutputHtml].templates[TemplateDoc],
				S("output/html/doc")));
	assert(!strcmp(gpaths.output[OutputHtml].templates[TemplatePreview],
				S("output/html/preview")));
	// this is a directory, but it's too complex to check if it's possible to
	// read from something beforehand
	assert(!strcmp(gpaths.output[OutputHtml].templates[TemplateIndex],
				S("output/html/index")));
	assert(gpaths.output[OutputHtml].templates[TemplateTagIndex] == NULL);

	assert(fs_add_output_template_dir("fakeoutput", S("output/fake")) == false);
	assert(fs_add_output_template_dir("gemini", S("output/empty")));
	// make sure nothing ended up in gemini
	for(int i = 0; i < TemplateLast; i++)
		assert(gpaths.output[OutputGemini].templates[i] == NULL);

	assert(fs_add_output_template_dir("html", S("output/html2")));
	assert(fs_add_output_template_dir("gemini", S("output/html2")));
	assert(fs_add_output_template("gemini", "index", S("output/html/index")));
	assert(fs_add_output_template("fakeoutput", "preview",
				S("output/fake/preview")) == false);
	assert(fs_add_output_template("gemini", "faketemplate",
				S("output/fake/preview")) == false);
	assert(fs_add_output_template("gemini", "preview",
				S("output/notafile/preview")) == false);

	assert(fs_add_template_dir(S("fake")) == false);
	assert(fs_add_template_dir(S("big")));

	assert(!strcmp(gpaths.output[OutputHtml].templates[TemplateDoc],
				S("big/html/doc")));
	assert(!strcmp(gpaths.output[OutputHtml].templates[TemplatePreview],
				S("output/html2/preview")));
	assert(!strcmp(gpaths.output[OutputHtml].templates[TemplateIndex],
				S("output/html2/index")));
	assert(!strcmp(gpaths.output[OutputGemini].templates[TemplatePreview],
				S("output/html2/preview")));
	assert(!strcmp(gpaths.output[OutputGemini].templates[TemplateIndex],
				S("output/html/index")));
	assert(!strcmp(gpaths.output[OutputGopher].templates[TemplatePreview],
				S("big/gopher/preview")));
	assert(!strcmp(gpaths.output[OutputGopher].templates[TemplateIndex],
				S("big/gopher/index")));
	assert(gpaths.output[OutputGopher].templates[TemplateDoc] == NULL);

	// make sure the fake output file didn't end up anywhere
	assert(!outstr_exists(S("output/fake/preview")));


	cleanup_files();
	fs_deinit();
}
