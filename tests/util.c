#include <assert.h>
#include <string.h>

#include "src/util.c"

int main()
{
	struct array array = array_init(sizeof(char));
	array_atleast_cap(&array, 0);
	assert(array.cap == 0);
	array_atleast_cap(&array, 4);
	assert(array.cap == 4);
	array_atleast_cap(&array, 2);

	memset(array_add(&array, 4), 2, 4);
	assert(array.cap == 4);
	memset(array_add(&array, 4), 3, 4);
	assert(array.cap == 8);

	array_atleast_cap(&array, 97);
	assert(array.cap == 8*2*2*2*2);
	array_atleast_cap(&array, 4096);
	assert(array.cap == 128*2*2*2*2*2);

	*((char *)array_add(&array, 1)) = 42;
	*((char *)array_add(&array, 1)) = 89;

	assert(array.len == 4+4+2);
	assert(!memcmp(array.data, (char[]){
		2, 2, 2, 2, 3, 3, 3, 3,
		42, 89,
	}, 4+4+2));

	array_rm_messy(&array, 0);
	array_rm_messy(&array, 8);
	array_rm_messy(&array, 4);

	assert(!memcmp(array.data, (char[]){
		89, 2, 2, 2, 3, 3, 3,
	}, 7));
}
