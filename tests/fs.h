static void make_files(void)
{
	int i;

	for(i = 0; i < (int)(sizeof(dirs)/sizeof(*dirs)); i++) {
		mkdir(dirs[i], 0777);
		assert(!(errno && errno != EEXIST));
	}
	for(i = 0; i < (int)(sizeof(files)/sizeof(*files)); i++) {
		int fd = open(files[i], O_CREAT, 0666);
		assert(fd);
		close(fd);
	}
}

static void cleanup_files(void)
{
	int i;

	for(i = (int)(sizeof(files)/sizeof(*files)) - 1; i >= 0; i--) {
		unlink(files[i]);
	}
	for(i = (int)(sizeof(dirs)/sizeof(*dirs)) - 1; i >= 0; i--) {
		rmdir(dirs[i]);
	}
}
