#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "src/fs.c"

#define S(x) \
	"/tmp/.test/" x

bool input_ok = true;
char *program_name;

char *dirs[] = {
	S(""),
	S("full"),
	S("full/a"),
	S("empty"),
	S("empty2"),
	S("empty2/a"),
	S("empty2/b.snx"),
	S("empty2/.snx"),
	S("empty2/.snx/c"),
	S("strange"),
	S("strange/dir.snx"),
	S("recurse"),
	S("recurse/1"),
	S("recurse/1/2"),
	S("recurse/1/2/3"),
	S("recurse/1/2/3/4"),
};

char *files[] = {
	S("file"),
	S("file2.snx"),
	S("full/a/1.snx"),
	S("full/a/2.snx"),
	S("full/3.snx"),
	S("strange/.snx"),
	S("strange/:snx"),
	S("strange/a:snx"),
	S("strange/a.snx"),
	S("strange/aa.m"),
	S("strange/a.me"),
	S("strange/dir.snx/file.snx"),
	S("strange/dir.snx/file2"),
	S("recurse/1/2/3/4/5.snx"),
};

#include "fs.h"

// a pointer we can set list indices to to indicate success
char *list_item_found = "found";
char **list;

bool test1(char *path)
{
	(void)path;

	// make sure src_file is a dir
	assert(strcmp(path, gpaths.src_from_cwd) != 0);

	for(int i = 0; list[i] != NULL; i++) {
		if(!strcmp(list[i], path)) {
			list[i] = list_item_found;
			return true;
		}
	}

	// the file isn't in the passed list
	assert(false);
}

bool test2(char *path)
{
	(void)path;
	assert(false);
}

bool test3(char *path)
{
	(void)path;
	return false;
}

// test if the fs_walk generates exclusively the items in the null-terminated
// new_list
bool test_with_list(char *path, char **new_list)
{
	list = new_list;
	assert(fs_set_src(path));
	if(!fs_walk_source(test1))
		return false;

	for(int i = 0; list[i] != NULL; i++)
		if(list[i] != list_item_found)
			return false;

	return true;
}

int main(int argc, char **argv)
{
	(void)argc;
	program_name = argv[0];

	fs_init();
	make_files();

	assert(test_with_list(S("full"), (char *[]){
		S("full/a/1.snx"),
		S("full/a/2.snx"),
		S("full/3.snx"),
		NULL,
	}));

	assert(test_with_list(S("empty"), (char *[]){
		NULL,
	}));

	assert(test_with_list(S("empty2"), (char *[]){
		NULL,
	}));

	assert(test_with_list(S("file"), (char *[]){
		S("file"),
		NULL,
	}) == false);

	assert(test_with_list(S("file2.snx"), (char *[]){
		S("file2.snx"),
		NULL,
	}) == false);

	assert(test_with_list(S("strange"), (char *[]){
		S("strange/a.snx"),
		S("strange/dir.snx/file.snx"),
		NULL,
	}));

	free(gpaths.src_from_cwd);
	gpaths.src_from_cwd = xstrdup(S("path"));
	assert(fs_walk_source(test2) == false);

	// test if passed function returning false works correctly
	assert(fs_set_src(S("recurse")));
	assert(fs_walk_source(test3) == false);

	cleanup_files();
	fs_deinit();
}
