#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <stdbool.h>
#include <unistd.h>

#include "src/document.c"

char *program_name;

#define IN_ARR_BOUNDS(arr, i) \
	( (i) < (arr).len )

#define STR_AFTER_STR(file, var1, var2) \
	( (var1) + strlen(&doc_file_head(file)[var1]) >= (var2) )

// make sure no pointers/strings look impossible
static void test_sane(struct doc_file *file)
{
	struct doc_var *vars = file->vars.data;

	for(size_t i = 0; i < file->vars.len; i++) {
		// make sure the var name isn't empty
		assert(strlen(doc_file_var(*file, i, name)) > 0);
		// make sure the value exists after the name
		assert(STR_AFTER_STR(*file, vars[i].val, vars[i].name));
		// make sure the name exists after the last var's value
		assert(i <= 0 || STR_AFTER_STR(*file, vars[i].name, vars[i-1].val));
		// make sure the name and value are within the buffer
		assert(IN_ARR_BOUNDS(file->head, vars[i].name));
		assert(IN_ARR_BOUNDS(file->head, vars[i].val));
	}

	// make sure the rest of the file is after the vars
	assert(file->vars.len == 0 ||
			STR_AFTER_STR(*file, file->rest, vars[file->vars.len-1].val));
	// make sure the rest of the file is in the buffer
	assert(IN_ARR_BOUNDS(file->head, file->rest));
}

static void load_doc_n(struct doc_file *doc_file, char *text, size_t len)
{
	int pipefd[2];
	FILE *in, *out;

	assert(pipe(pipefd) == 0);
	in = fdopen(pipefd[1], "w");
	out = fdopen(pipefd[0], "r");
	assert(in != NULL);
	assert(out != NULL);

	fwrite(text, 1, len, in);

	fclose(in);
	assert(doc_file_load(out, "", doc_file));
	fclose(out);
	test_sane(doc_file);
}

static void load_doc(struct doc_file *doc_file, char *text)
{
	load_doc_n(doc_file, text, strlen(text));
}

int main(int argc, char **argv)
{
	(void)argc;
	program_name = argv[0];

	struct doc_file doc_file;

	doc_file = doc_file_init();

#define TEST_VAR(i, n, v) \
	assert(!strcmp(doc_file_var(doc_file, i, name), n)); \
	assert(!strcmp(doc_file_var(doc_file, i, val), v))

#define TEST_REST(val) \
	assert(!strcmp(doc_file_rest(doc_file), val))

	load_doc(&doc_file, "");
	assert(doc_file.vars.len == 0);
	TEST_REST("");

	load_doc(&doc_file, "no vars");
	assert(doc_file.vars.len == 0);
	TEST_REST("no vars");

	load_doc(&doc_file, "|one: var");
	assert(doc_file.vars.len == 1);
	TEST_VAR(0, "one", "var");
	TEST_REST("");

	load_doc(&doc_file,
		"| One1: var\n"
		"|bad var\n"
		"|two:var \n"
		"|\tbad : var\n"
		"|\n"
		"|:\n"
		"| :\n"
		"| : \n"
		"| :bad\n"
		"|three: \t with  some spaces    \n"
		"|  blank1:\n"
		"|  blank2:  \n"
		"|\n"
		"ending text\n"
	);
	assert(doc_file.vars.len == 5);

	TEST_VAR(0, "One1", "var");
	TEST_VAR(1, "two", "var");
	TEST_VAR(2, "three", "with  some spaces");
	TEST_VAR(3, "blank1", "");
	TEST_VAR(4, "blank2", "");
	TEST_REST("ending text\n");

	load_doc(&doc_file, "|blank3:");
	assert(doc_file.vars.len == 1);
	TEST_VAR(0, "blank3", "");
	TEST_REST("");

	load_doc(&doc_file, "|blank4:\t");
	assert(doc_file.vars.len == 1);
	TEST_VAR(0, "blank4", "");
	TEST_REST("");

	load_doc_n(&doc_file, "|nu\0ll:val\0ue", sizeof("|nu\0ll:val\0ue"));
	assert(doc_file.vars.len == 1);
	TEST_VAR(0, "null", "value");

	doc_file_deinit(&doc_file);
}
