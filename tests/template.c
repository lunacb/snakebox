#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include "util.h"

#include "src/template.c"

char *program_name;

static inline bool sliceeq(struct slice *slice, char *str)
{
	return !strncmp(slice->start, str, slice->len);
}

static inline struct template_ctx make(struct template_walker *walker, char *text)
{
	int pipefd[2];
	assert(pipe(pipefd) == 0);
	FILE *in = fdopen(pipefd[1], "w");
	FILE *out = fdopen(pipefd[0], "r");
	assert(in != NULL);
	assert(out != NULL);
	assert(fprintf(in, text) != -1);
	fclose(in);
	struct template_ctx res;
	assert(template_init(&res, out));
	fclose(out);
	*walker = template_walker_init(&res);
	return res;
}

int main(int argc, char **argv)
{
	(void)argc;
	program_name = argv[0];

	struct template_ctx ctx;
	struct template_walker walker, walker2, walker3;
	struct template_segment *seg;

	ctx = make(&walker,
			"haiii @@{uwu}@@\n"
			"list time! @@[list|inner]@@[delim]@@"
			);

	assert((seg = template_walk(&walker)) != NULL);
	assert(seg->type == TemplateText);
	assert(sliceeq(&seg->text, "haiii "));

	assert((seg = template_walk(&walker)) != NULL);
	assert(seg->type == TemplateVar);
	assert(!strcmp(seg->var.name, "uwu"));

	assert((seg = template_walk(&walker)) != NULL);
	assert(seg->type == TemplateText);
	assert(sliceeq(&seg->text, "\nlist time! "));

	assert((seg = template_walk(&walker)) != NULL);
	assert(seg->type == TemplateArray);

	{
		walker2 = template_walker_init_array(&ctx, seg);

		assert((seg = template_walk(&walker2)) != NULL);
		assert(seg->type == TemplateText);
		assert(sliceeq(&seg->text, "inner"));

		assert((seg = template_walk(&walker2)) == NULL);

		for(int i = 0; i < 5; i++) {
			assert((seg = template_walk(&walker2)) != NULL);
			assert(seg->type == TemplateText);
			assert(sliceeq(&seg->text, "delim"));

			assert((seg = template_walk(&walker2)) != NULL);
			assert(seg->type == TemplateText);
			assert(sliceeq(&seg->text, "inner"));

			assert((seg = template_walk(&walker2)) == NULL);
		}
	}

	assert((seg = template_walk(&walker)) == NULL);

	template_deinit(&ctx);

	ctx = make(&walker,
			"@@[one|\n"
			"  text @@{var}@@@@[two|\n"
			"    @@{var}@@ text\n"
			"  ]@@[\n"
			"    text @@{var}@@\n"
			"  ]@@]@@"
			);

	assert((seg = template_walk(&walker)) != NULL);
	assert(seg->type == TemplateArray);

	{
		walker2 = template_walker_init_array(&ctx, seg);

		assert((seg = template_walk(&walker2)) != NULL);
		assert(seg->type == TemplateText);
		assert(sliceeq(&seg->text, "\n  text "));

		assert((seg = template_walk(&walker2)) != NULL);
		assert(seg->type == TemplateVar);
		assert(!strcmp(seg->var.name, "var"));

		assert((seg = template_walk(&walker2)) != NULL);
		assert(seg->type == TemplateArray);

		{
			walker3 = template_walker_init_array(&ctx, seg);

			assert((seg = template_walk(&walker3)) != NULL);
			assert(seg->type == TemplateText);
			assert(sliceeq(&seg->text, "\n    "));

			assert((seg = template_walk(&walker3)) != NULL);
			assert(seg->type == TemplateVar);
			assert(!strcmp(seg->var.name, "var"));

			assert((seg = template_walk(&walker3)) != NULL);
			assert(seg->type == TemplateText);
			assert(sliceeq(&seg->text, " text\n  "));

			assert((seg = template_walk(&walker3)) == NULL);

			for(int i = 0; i < 5; i++) {
				assert((seg = template_walk(&walker3)) != NULL);
				assert(seg->type == TemplateText);
				assert(sliceeq(&seg->text, "\n    text "));

				assert((seg = template_walk(&walker3)) != NULL);
				assert(seg->type == TemplateVar);
				assert(!strcmp(seg->var.name, "var"));

				assert((seg = template_walk(&walker3)) != NULL);
				assert(seg->type == TemplateText);
				assert(sliceeq(&seg->text, "\n  "));

				assert((seg = template_walk(&walker3)) != NULL);
				assert(seg->type == TemplateText);
				assert(sliceeq(&seg->text, "\n    "));

				assert((seg = template_walk(&walker3)) != NULL);
				assert(seg->type == TemplateVar);
				assert(!strcmp(seg->var.name, "var"));

				assert((seg = template_walk(&walker3)) != NULL);
				assert(seg->type == TemplateText);
				assert(sliceeq(&seg->text, " text\n  "));

				assert((seg = template_walk(&walker3)) == NULL);
			}
		}

		assert((seg = template_walk(&walker2)) == NULL);
	}

	assert((seg = template_walk(&walker)) == NULL);

	template_deinit(&ctx);
}
