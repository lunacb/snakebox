#define _POSIX_C_SOURCE 200809L

#include  <dirent.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>

#include "src/fs.c"

#define S(x) \
	"/tmp/.test/" x

bool input_ok = true;
char *program_name;

char *dirs[] = {
	S(""),
	S("src"),
	S("src/dir"),
	S("dest"),
	S("dest/one"),
	S("dest/some"),
	S("dest/more"),
	S("dest/more/gemini"),
	S("test"),
	S("test/third.snx"),
	S("make/"),
	S("make/one"),
};

char *files[] = {
	S("src/file"),
	S("dest/one_file"),
	S("dest/some/gemini"),
	S("dest/more/html"),
	S("dest/more/gopher"),
	S("test/first"),
	S("test/.snx"),
	S("make/file"),
};

#include "fs.h"

static bool make_basedir(char *path)
{
	char *str = strdup(path);
	assert(str);
	bool res = fs_make_basedir(str);
	free(str);
	return res;
}

static bool test_basedir(char *path, char *dir, bool empty)
{
	if(!make_basedir(path))
		return false;
	DIR *dirp = opendir(dir);
	if(dirp == NULL)
		return false;

	struct dirent *dp;
	while((dp = readdir(dirp)) != NULL) {
		if(!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue;
		closedir(dirp);
		return !empty;
	}

	closedir(dirp);

	if(errno || !empty)
		return false;

	return true;
}

int main(int argc, char **argv)
{
	(void)argc;
	program_name = argv[0];

	fs_init();
	make_files();

	assert(fs_set_src(S("src/file")));
	assert(fs_set_src(S("src/dir")));
	assert(fs_set_src(S("src/fake")) == true);
	assert(!strcmp(gpaths.src_from_cwd, S("src/fake")));

	assert(fs_set_output_dest("html", S("dest/one")));
	assert(fs_set_output_dest("gopher", S("dest/one_file")));

	assert(fs_set_output_dest("fake", S("dest/one_file")) == false);
	assert(fs_set_output_dest("gopher", S("dest/fake_file")) == true);

	assert(fs_set_dest(S("dest/some")));
	assert(fs_set_dest(S("dest/fake_some")) == false);

#define dest_file(out) \
	gpaths.output[out].dest_from_cwd

	assert(!strcmp(dest_file(OutputHtml), S("dest/some/html")));
	assert(!strcmp(dest_file(OutputGopher), S("dest/some/gopher")));
	assert(!strcmp(dest_file(OutputGemini), S("dest/some/gemini")));

	assert(fs_set_dest(S("dest/more")));

	assert(!strcmp(dest_file(OutputHtml), S("dest/more/html")));
	assert(!strcmp(dest_file(OutputGopher), S("dest/more/gopher")));
	assert(!strcmp(dest_file(OutputGemini), S("dest/more/gemini")));

	assert(fs_set_output_dest("html", S("dest")));
	char *ptr, *ptr2;
	assert(fs_set_src(S("test/first")));

#define test_dest(src, res, partial) \
	ptr = fs_src_to_dest(src, OutputHtml, &ptr2); \
	assert(!strcmp(ptr, res)); \
	assert(!strcmp(ptr2, partial)); \
	free(ptr)

	test_dest(S("test/first/second.snx"), S("dest/second.html"), "second.html");
	test_dest(S("test/first/second"), S("dest/second.html"), "second.html");
	assert(fs_set_src(S("test/.snx")));
	test_dest(S("test/.snx/second.snx"), S("dest/second.html"), "second.html");
	test_dest(S("test/.snx/.snx"), S("dest/.snx.html"), ".snx.html");

#define test_gopher(src, res, partial) \
	ptr = fs_src_to_dest(src, OutputGopher, &ptr2); \
	assert(!strcmp(ptr, res)); \
	assert(!strcmp(ptr2, partial)); \
	free(ptr)

	test_gopher(S("test/.snx/second.snx"), S("dest/more/gopher/second/gophermap"), "second");

#define test_rel_dest(src, suffix, res, partial) \
	ptr = fs_rel_src_to_dest(src, suffix, OutputHtml, &ptr2); \
	assert(!strcmp(ptr, res)); \
	assert(!strcmp(ptr2, partial)); \
	free(ptr)

	assert(fs_set_output_dest("html", S("dest")));
	test_rel_dest("file", NULL, S("dest/file.html"), "file.html");
	test_rel_dest("file", ".suffix", S("dest/file.suffix"), "file.suffix");
	test_rel_dest("", NULL, S("dest/.html"), ".html");

	assert(make_basedir(S("make/file/last")) == false);
	assert(make_basedir(S("make/file/last/")) == false);
	assert(test_basedir(S("make/last"), S("make"), false));
	assert(test_basedir(S("make/one/last"), S("make/one"), true));
	assert(test_basedir(S("make///one/l/"), S("make/one"), true));
	assert(test_basedir(S("make///1/2/3/4/5"), S("make/1/2/3/4"), true));
	rmdir(S("make/1/2/3/4"));
	rmdir(S("make/1/2/3"));
	rmdir(S("make/1/2"));
	rmdir(S("make/1"));

#define test_rel_path(path, rel, res) \
	ptr = fs_dest_rel_path(path, rel, true, OutputLast); \
	assert(!strcmp(ptr, res)); \
	free(ptr)

	test_rel_path("a/b", "a/c", "c");
	test_rel_path("a/b/c", "a/d", "../d");
	test_rel_path("a", "b", "b");
	test_rel_path("aa/bb", "aa/cc", "cc");
	test_rel_path("aa/bb/cc", "aa/d", "../d");
	test_rel_path("aa", "bb", "bb");
	test_rel_path("a/b/c/", "a/b/d", "d");
	test_rel_path("a/b/c", "a/b/d/", "d/");
	test_rel_path("a/b/c/d/e/f/g", "a/h", "../../../../../h");
	test_rel_path("a/b", "a/c/d/e/f/g", "c/d/e/f/g");
	test_rel_path("a/b", "a/b", "b");
	test_rel_path("1111/2222", "1111/2xxx", "2xxx");

	cleanup_files();
	fs_deinit();
}
