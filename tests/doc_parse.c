#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "sliceop.h"

#include "src/document.c"

char *program_name;

int main(int argc, char **argv)
{
	(void)argc;
	program_name = argv[0];

	struct doc_iter iter;
	struct doc_stack_token *tok;
	bool is_reused;

#define TEST_SLICE(slice, str) \
	assert((slice).len == strlen(str)); \
	assert(!strncmp((slice).start, str, (slice).len))

#define TEST_GENERIC(n, _type) \
	assert(doc_iter_walk(&iter, &tok, &is_reused) == n); \
	assert(tok->token.type == _type)

#define TEST_END() \
	assert(doc_iter_walk(&iter, &tok, &is_reused) == 0)

#define TEST_TEXT(n, _flags, _full_flags, _text, _is_reused) \
	TEST_GENERIC(n, DocText); \
	assert(tok->token.text.flags == (_flags)); \
	assert(tok->token.text.full_flags == (_full_flags)); \
	TEST_SLICE(tok->token.text.text, _text); \
	assert(is_reused == _is_reused)

#define TEST_ORDERED(n, _number) \
	TEST_GENERIC(n, DocOrderedItem); \
	assert(tok->token.ordered.number == _number)

#define TEST_PATH(n, _path) \
	TEST_GENERIC(n, DocPath); \
	TEST_SLICE(tok->token.path.path, _path)

#define TEST_EMBED(n, _path) \
	TEST_GENERIC(n, DocEmbed); \
	TEST_SLICE(tok->token.path.path, _path)

#define TEST_HEADING(n, _level) \
	TEST_GENERIC(n, DocHeading); \
	assert(tok->token.heading.level == _level)

#define TEST_PARAGRAPH(text) \
	TEST_GENERIC(1, DocParagraph); \
	TEST_TEXT(1, 0, 0, text, false); \
	TEST_TEXT(-1, 0, 0, text, false); \
	TEST_GENERIC(-1, DocParagraph)

#define TEST_QUOTE_PARAGRAPH(level, text) \
	assert(iter.quote_level == level); \
	TEST_GENERIC(1, DocParagraph); \
	TEST_TEXT(1, 0, 0, text, false); \
	TEST_TEXT(-1, 0, 0, text, false); \
	TEST_GENERIC(-1, DocParagraph)


	iter = doc_make_iter_slice(
			(struct slice)str_slice(
		"\n    # heading\n"
		"###    head[^b]{BOLD}ing\n"
		"[# 69]{heading}"
		"[paragraph]  :some text [p] : some more\n"
		"[p]{\n"
		"even more text\n"
		"}\n"
		"     \n\n\n\n\n"
		"[L] \t{\n"
		"* dot with [^B]:BOLD and [^I]:ITALIC\n"
		"[*]: dot with [^B]{DOT [^italic]{THING}S}\n\n\n"
		"}\n"
		"[O]{\n"
		"* an ordered item\n"
		"[*]: another ordered item\n"
		"}\n"
		"[ =>  link ]: text\n\n"
		"[>> embed  ] { texxt}\n"
		"[p]: extra\n"
		"[q] {\n"
			"[p]:text\n"
			"[q] {\n"
				"[p]:text2\n"
			"}\n"
			"[p]:text3\n"
		"}"

	), 1, "name");

	TEST_HEADING(1, 1);
	TEST_TEXT(1, 0, 0, "heading", false);
	TEST_TEXT(-1, 0, 0, "heading", false);
	TEST_HEADING(-1, 1);

	TEST_HEADING(1, 3);
	TEST_TEXT(1, 0, 0, "head", false);
	TEST_TEXT(1, DocTextBold, DocTextBold, "BOLD", false);
	TEST_TEXT(-1, DocTextBold, DocTextBold, "BOLD", false);
	TEST_TEXT(1, 0, 0, "ing", true);
	TEST_TEXT(-1, 0, 0, "ing", false);
	TEST_HEADING(-1, 3);

	TEST_HEADING(1, 69);
	TEST_TEXT(1, 0, 0, "heading", false);
	TEST_TEXT(-1, 0, 0, "heading", false);
	TEST_HEADING(-1, 69);

	TEST_PARAGRAPH("some text");
	TEST_PARAGRAPH("some more");
	TEST_PARAGRAPH("even more text");

	TEST_GENERIC(1, DocList);

	TEST_GENERIC(1, DocListItem);
	TEST_TEXT(1, 0, 0, "dot with", false);
	TEST_TEXT(1, DocTextBold, DocTextBold, "BOLD and", false);
	TEST_TEXT(-1, DocTextBold, DocTextBold, "BOLD and", false);
	TEST_TEXT(1, DocTextItalic, DocTextItalic, "ITALIC", false);
	TEST_TEXT(-1, DocTextItalic, DocTextItalic, "ITALIC", false);
	TEST_TEXT(-1, 0, 0, "dot with", false);
	TEST_GENERIC(-1, DocListItem);

	TEST_GENERIC(1, DocListItem);
	TEST_TEXT(1, 0, 0, "dot with", false);
	TEST_TEXT(1, DocTextBold, DocTextBold, "DOT", false);
	TEST_TEXT(1, DocTextItalic, DocTextBold | DocTextItalic,
			"THING", false);
	TEST_TEXT(-1, DocTextItalic, DocTextBold | DocTextItalic,
			"THING", false);
	TEST_TEXT(1, DocTextBold, DocTextBold, "S", true);
	TEST_TEXT(-1, DocTextBold, DocTextBold, "S", false);
	TEST_TEXT(-1, 0, 0, "dot with", false);
	TEST_GENERIC(-1, DocListItem);

	TEST_GENERIC(-1, DocList);

	TEST_GENERIC(1, DocOrderedList);

	TEST_ORDERED(1, 1);
	TEST_TEXT(1, 0, 0, "an ordered item", false);
	TEST_TEXT(-1, 0, 0, "an ordered item", false);
	TEST_ORDERED(-1, 1);

	TEST_ORDERED(1, 2);
	TEST_TEXT(1, 0, 0, "another ordered item", false);
	TEST_TEXT(-1, 0, 0, "another ordered item", false);
	TEST_ORDERED(-1, 2);

	TEST_GENERIC(-1, DocOrderedList);

	TEST_PATH(1, "link");
	TEST_TEXT(1, 0, 0, "text", false);
	TEST_TEXT(-1, 0, 0, "text", false);
	TEST_PATH(-1, "link");

	TEST_EMBED(1, "embed");
	TEST_TEXT(1, 0, 0, "texxt", false);
	TEST_TEXT(-1, 0, 0, "texxt", false);
	TEST_EMBED(-1, "embed");

	TEST_PARAGRAPH("extra");

	TEST_GENERIC(1, DocQuote);
	TEST_QUOTE_PARAGRAPH(1, "text");
	TEST_GENERIC(1, DocQuote);
	TEST_QUOTE_PARAGRAPH(2, "text2");
	TEST_GENERIC(-1, DocQuote);
	TEST_QUOTE_PARAGRAPH(1, "text3");
	TEST_GENERIC(-1, DocQuote);

	TEST_END();

#define TEST_BAD(bad) \
	iter = doc_make_iter_slice( \
			(struct slice)str_slice( \
		bad \
	), 1, "name"); \
	TEST_END()

	TEST_BAD("}");
	TEST_BAD("\n}");
	TEST_BAD("text");
	TEST_BAD("[ fake]:");
	TEST_BAD("[L extra]:");
	TEST_BAD("[p extra]:");
	TEST_BAD("[# extra extra]:");
	TEST_BAD("[# extra extra]:");
	TEST_BAD("[# extra]:");
	TEST_BAD("[=> extra extra]:");
	TEST_BAD("[>> extra extra]:");
	TEST_BAD("[=> ]:");
	TEST_BAD("[>>]:");
	TEST_BAD("[o 1]:");
	TEST_BAD(" [*] a");
	TEST_BAD(" * a");
	TEST_BAD(" 1) a");
}
